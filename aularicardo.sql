-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2020 a las 14:46:57
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aularicardo`
--
CREATE DATABASE IF NOT EXISTS `aularicardo` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `aularicardo`;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `barra`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `barra` (
`sexo` varchar(1)
,`AVG(realizado.nota)` double
,`id_examen` int(10)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `barra1`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `barra1` (
`sexo` varchar(1)
,`promedio` double
,`id_examen` int(10)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `dona`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `dona` (
`nombre` varchar(30)
,`promedio` varchar(54)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--

CREATE TABLE `examen` (
  `id_examen` int(10) NOT NULL,
  `titulo` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `id_profesor` int(10) NOT NULL,
  `id_asignatura` int(10) NOT NULL,
  `id_seccion` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `lapso` int(10) NOT NULL,
  `fechafin` datetime NOT NULL,
  `tiempo` time NOT NULL,
  `ponde1` int(3) NOT NULL,
  `ponde2` int(3) NOT NULL,
  `ponde3` int(3) NOT NULL,
  `practica` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `examenesprofesor`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `examenesprofesor` (
`id_realizado` int(10)
,`nota` float
,`tt` time
,`titulo` varchar(350)
,`id_usuario` int(10)
,`seccion` varchar(30)
,`ci` varchar(20)
,`practica` int(2)
,`Nombre` varchar(411)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fases`
--

CREATE TABLE `fases` (
  `id_fase` int(10) NOT NULL,
  `titulo` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `id_examen` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `line`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `line` (
`t1` time(4)
,`t2` time(4)
,`t3` time(4)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `id_pregunta` int(10) NOT NULL,
  `id_examen` int(10) NOT NULL,
  `id_fase` int(10) NOT NULL,
  `pregunta` text COLLATE utf8_spanish_ci NOT NULL,
  `id_respuesta` int(10) NOT NULL,
  `img` longblob NOT NULL,
  `respuesta` varchar(350) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `preguntassinresponder`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `preguntassinresponder` (
`id_pregunta` int(10)
,`id_examen` int(10)
,`id_fase` int(10)
,`pregunta` text
,`id_respuesta` int(10)
,`estudiante` int(10)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `realizado`
--

CREATE TABLE `realizado` (
  `id_realizado` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `id_examen` int(10) NOT NULL,
  `nota` float NOT NULL,
  `tt` time NOT NULL,
  `t1` time NOT NULL,
  `t2` time NOT NULL,
  `t3` time NOT NULL,
  `bien` int(10) NOT NULL,
  `mal` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestas`
--

CREATE TABLE `repuestas` (
  `id_respuesta` int(10) NOT NULL,
  `id_examen` int(10) NOT NULL,
  `id_fase` int(10) NOT NULL,
  `id_pregunta` int(10) NOT NULL,
  `repuesta` text COLLATE utf8_spanish_ci NOT NULL,
  `vf` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestasestudiantes`
--

CREATE TABLE `repuestasestudiantes` (
  `id_respuestaE` int(10) NOT NULL,
  `id_pregunta` int(10) NOT NULL,
  `id_estudiante` int(10) NOT NULL,
  `id_respuesta` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE `sesiones` (
  `id` int(10) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempos`
--

CREATE TABLE `tiempos` (
  `id_tiempo` int(10) NOT NULL,
  `id_estudiante` int(10) NOT NULL,
  `id_examen` int(10) NOT NULL,
  `tf0` time NOT NULL,
  `tf1` time NOT NULL,
  `tf2` time NOT NULL,
  `tf3` time NOT NULL,
  `tt` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(10) NOT NULL,
  `nombre` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `ci` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fechaN` date NOT NULL,
  `sexo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `seccion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `year` date NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `id_asignatura` int(10) NOT NULL,
  `tipo` int(10) NOT NULL,
  `pass` varchar(350) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellido`, `ci`, `fechaN`, `sexo`, `seccion`, `year`, `correo`, `direccion`, `tlf`, `id_asignatura`, `tipo`, `pass`) VALUES
(9, 'John', 'Donga', 'V-1000000', '1997-12-18', 'F', '1', '0000-00-00', 'john.donga@egrappler.com', 'caracas,municipio libertador,parroquia santa teresa calle carcel a monzon', '(0414) 366-3615', 0, 2, 'e10adc3949ba59abbe56e057f20f883e'),
(12, 'admin', 'Donga', 'V-1000000', '1988-01-30', 'F', '1', '0000-00-00', 'admin@admin.com', 'caracas,municipio libertador,parroquia santa teresa calle carcel a monzon', '(0414) 366-3615', 0, 3, 'e10adc3949ba59abbe56e057f20f883e'),
(13, 'eugenio', 'Donga', 'V-1000000', '2000-02-24', 'M', '1', '0000-00-00', 'eugenio@gmail.com', 'qwerty', '(0414) 366-3615', 0, 1, 'e10adc3949ba59abbe56e057f20f883e'),
(14, 'John1', 'Donga13132', 'V-25897751', '2020-04-10', 'M', '2', '0000-00-00', 'john.don112ga@egrappler.com', 'caracas,municipio libertador,parroquia santa teresa calle carcel a monzon', '(0414) 366-3615', 0, 2, '827ccb0eea8a706c4c34a16891f84e7b'),
(15, 'Jose miguel', 'gimenez', 'V-25897751', '2011-04-29', 'M', '7', '0000-00-00', 'josemiguel@gmail.com', 'caracas,municipio libertador,parroquia santa teresa calle carcel a monzon', '(0414) 366-3615', 0, 1, 'e10adc3949ba59abbe56e057f20f883e'),
(16, 'Johnmlml', 'Donga', 'V-1000000', '2020-04-20', 'F', '7', '0000-00-00', 'john.donga1010@egrappler.com', 'Calle San Antonio Edificio Manaure Piso 11 Apto 112, Bulevar de Sabana Grande - Caracas', '(0414) 366-3615', 0, 1, 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `verticales`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `verticales` (
`titulo` varchar(350)
,`promedio` varchar(54)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_examen`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_examen` (
`id_examen` int(10)
,`practica` int(2)
,`titulo` varchar(350)
,`id_profesor` int(10)
,`id_asignatura` int(10)
,`id_seccion` int(10)
,`fecha` date
,`lapso` int(10)
,`fechafin` datetime
,`tiempo` time
,`id` int(10)
,`nombre` varchar(30)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_usuarios`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_usuarios` (
`id_usuario` int(10)
,`nombre` varchar(350)
,`ci` varchar(20)
,`pass` varchar(350)
,`apellido` varchar(60)
,`fechaN` date
,`sexo` varchar(1)
,`seccion` varchar(30)
,`correo` varchar(30)
,`tlf` varchar(15)
,`tipo` int(10)
,`direccion` text
,`tipoUsuario` varchar(13)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `barra`
--
DROP TABLE IF EXISTS `barra`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `barra`  AS  select `usuarios`.`sexo` AS `sexo`,avg(`realizado`.`nota`) AS `AVG(realizado.nota)`,`realizado`.`id_examen` AS `id_examen` from ((`usuarios` join `realizado` on((`usuarios`.`id_usuario` = `realizado`.`id_usuario`))) join `examen` on((`realizado`.`id_examen` = `examen`.`id_examen`))) group by `usuarios`.`sexo` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `barra1`
--
DROP TABLE IF EXISTS `barra1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `barra1`  AS  select `usuarios`.`sexo` AS `sexo`,avg(`realizado`.`nota`) AS `promedio`,`examen`.`id_examen` AS `id_examen` from ((`examen` join `realizado` on((`examen`.`id_examen` = `realizado`.`id_examen`))) join `usuarios` on((`realizado`.`id_usuario` = `usuarios`.`id_usuario`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `dona`
--
DROP TABLE IF EXISTS `dona`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dona`  AS  select `sesiones`.`nombre` AS `nombre`,format(avg(`realizado`.`nota`),2) AS `promedio` from ((`realizado` join `examen` on((`realizado`.`id_examen` = `examen`.`id_examen`))) join `sesiones` on((`examen`.`id_seccion` = `sesiones`.`id`))) where (`examen`.`practica` = 0) group by `sesiones`.`id` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `examenesprofesor`
--
DROP TABLE IF EXISTS `examenesprofesor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `examenesprofesor`  AS  select `realizado`.`id_realizado` AS `id_realizado`,`realizado`.`nota` AS `nota`,`realizado`.`tt` AS `tt`,`examen`.`titulo` AS `titulo`,`realizado`.`id_usuario` AS `id_usuario`,`usuarios`.`seccion` AS `seccion`,`usuarios`.`ci` AS `ci`,`examen`.`practica` AS `practica`,concat(`usuarios`.`nombre`,' ',`usuarios`.`apellido`) AS `Nombre` from ((`realizado` join `examen` on((`realizado`.`id_examen` = `examen`.`id_examen`))) join `usuarios` on((`realizado`.`id_usuario` = `usuarios`.`id_usuario`))) where (`examen`.`id_seccion` = `usuarios`.`seccion`) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `line`
--
DROP TABLE IF EXISTS `line`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `line`  AS  select sec_to_time(avg(time_to_sec(`realizado`.`t1`))) AS `t1`,sec_to_time(avg(time_to_sec(`realizado`.`t2`))) AS `t2`,sec_to_time(avg(time_to_sec(`realizado`.`t3`))) AS `t3` from (`realizado` join `examen` on((`realizado`.`id_examen` = `examen`.`id_examen`))) where (`examen`.`practica` = 0) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `preguntassinresponder`
--
DROP TABLE IF EXISTS `preguntassinresponder`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `preguntassinresponder`  AS  select `preguntas`.`id_pregunta` AS `id_pregunta`,`preguntas`.`id_examen` AS `id_examen`,`preguntas`.`id_fase` AS `id_fase`,`preguntas`.`pregunta` AS `pregunta`,`preguntas`.`id_respuesta` AS `id_respuesta`,`repuestasestudiantes`.`id_estudiante` AS `estudiante` from (`preguntas` left join `repuestasestudiantes` on((`preguntas`.`id_pregunta` = `repuestasestudiantes`.`id_pregunta`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `verticales`
--
DROP TABLE IF EXISTS `verticales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `verticales`  AS  select `examen`.`titulo` AS `titulo`,format(avg(`realizado`.`nota`),2) AS `promedio` from ((`realizado` join `examen` on((`realizado`.`id_examen` = `examen`.`id_examen`))) join `sesiones` on((`examen`.`id_seccion` = `sesiones`.`id`))) group by `examen`.`id_examen` order by format(avg(`realizado`.`nota`),2) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_examen`
--
DROP TABLE IF EXISTS `vista_examen`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_examen`  AS  select `examen`.`id_examen` AS `id_examen`,`examen`.`practica` AS `practica`,`examen`.`titulo` AS `titulo`,`examen`.`id_profesor` AS `id_profesor`,`examen`.`id_asignatura` AS `id_asignatura`,`examen`.`id_seccion` AS `id_seccion`,`examen`.`fecha` AS `fecha`,`examen`.`lapso` AS `lapso`,`examen`.`fechafin` AS `fechafin`,`examen`.`tiempo` AS `tiempo`,`sesiones`.`id` AS `id`,`sesiones`.`nombre` AS `nombre` from (`examen` join `sesiones` on((`examen`.`id_seccion` = `sesiones`.`id`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_usuarios`
--
DROP TABLE IF EXISTS `vista_usuarios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_usuarios`  AS  select `usuarios`.`id_usuario` AS `id_usuario`,`usuarios`.`nombre` AS `nombre`,`usuarios`.`ci` AS `ci`,`usuarios`.`pass` AS `pass`,`usuarios`.`apellido` AS `apellido`,`usuarios`.`fechaN` AS `fechaN`,`usuarios`.`sexo` AS `sexo`,`sesiones`.`nombre` AS `seccion`,`usuarios`.`correo` AS `correo`,`usuarios`.`tlf` AS `tlf`,`usuarios`.`tipo` AS `tipo`,`usuarios`.`direccion` AS `direccion`,(case when (`usuarios`.`tipo` = 1) then 'Estudiante' when (`usuarios`.`tipo` = 2) then 'Profesor' when (`usuarios`.`tipo` = 3) then 'Administrador' else 'indefinido' end) AS `tipoUsuario` from (`usuarios` join `sesiones` on((`usuarios`.`seccion` = `sesiones`.`id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `examen`
--
ALTER TABLE `examen`
  ADD PRIMARY KEY (`id_examen`),
  ADD KEY `id_profesor` (`id_profesor`),
  ADD KEY `id_asignatura` (`id_asignatura`),
  ADD KEY `id_seccion` (`id_seccion`);

--
-- Indices de la tabla `fases`
--
ALTER TABLE `fases`
  ADD PRIMARY KEY (`id_fase`),
  ADD KEY `id_examen` (`id_examen`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`id_pregunta`),
  ADD KEY `id_examen` (`id_examen`),
  ADD KEY `id_fase` (`id_fase`),
  ADD KEY `id_respuesta` (`id_respuesta`);

--
-- Indices de la tabla `realizado`
--
ALTER TABLE `realizado`
  ADD PRIMARY KEY (`id_realizado`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_examen` (`id_examen`);

--
-- Indices de la tabla `repuestas`
--
ALTER TABLE `repuestas`
  ADD PRIMARY KEY (`id_respuesta`),
  ADD KEY `id_examen` (`id_examen`),
  ADD KEY `id_fase` (`id_fase`),
  ADD KEY `id_pregunta` (`id_pregunta`);

--
-- Indices de la tabla `repuestasestudiantes`
--
ALTER TABLE `repuestasestudiantes`
  ADD PRIMARY KEY (`id_respuestaE`),
  ADD KEY `id_pregunta` (`id_pregunta`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_respuesta` (`id_respuesta`);

--
-- Indices de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tiempos`
--
ALTER TABLE `tiempos`
  ADD PRIMARY KEY (`id_tiempo`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_examen` (`id_examen`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_asignatura` (`id_asignatura`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `examen`
--
ALTER TABLE `examen`
  MODIFY `id_examen` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fases`
--
ALTER TABLE `fases`
  MODIFY `id_fase` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `id_pregunta` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `realizado`
--
ALTER TABLE `realizado`
  MODIFY `id_realizado` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `repuestas`
--
ALTER TABLE `repuestas`
  MODIFY `id_respuesta` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `repuestasestudiantes`
--
ALTER TABLE `repuestasestudiantes`
  MODIFY `id_respuestaE` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tiempos`
--
ALTER TABLE `tiempos`
  MODIFY `id_tiempo` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `fases`
--
ALTER TABLE `fases`
  ADD CONSTRAINT `fases_ibfk_1` FOREIGN KEY (`id_examen`) REFERENCES `examen` (`id_examen`);

--
-- Filtros para la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD CONSTRAINT `preguntas_ibfk_1` FOREIGN KEY (`id_examen`) REFERENCES `examen` (`id_examen`);

--
-- Filtros para la tabla `realizado`
--
ALTER TABLE `realizado`
  ADD CONSTRAINT `realizado_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`),
  ADD CONSTRAINT `realizado_ibfk_2` FOREIGN KEY (`id_examen`) REFERENCES `examen` (`id_examen`);

--
-- Filtros para la tabla `repuestas`
--
ALTER TABLE `repuestas`
  ADD CONSTRAINT `repuestas_ibfk_1` FOREIGN KEY (`id_examen`) REFERENCES `examen` (`id_examen`);

--
-- Filtros para la tabla `repuestasestudiantes`
--
ALTER TABLE `repuestasestudiantes`
  ADD CONSTRAINT `repuestasestudiantes_ibfk_1` FOREIGN KEY (`id_respuesta`) REFERENCES `repuestas` (`id_respuesta`),
  ADD CONSTRAINT `repuestasestudiantes_ibfk_2` FOREIGN KEY (`id_pregunta`) REFERENCES `preguntas` (`id_pregunta`),
  ADD CONSTRAINT `repuestasestudiantes_ibfk_3` FOREIGN KEY (`id_estudiante`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `tiempos`
--
ALTER TABLE `tiempos`
  ADD CONSTRAINT `tiempos_ibfk_1` FOREIGN KEY (`id_examen`) REFERENCES `examen` (`id_examen`),
  ADD CONSTRAINT `tiempos_ibfk_2` FOREIGN KEY (`id_estudiante`) REFERENCES `usuarios` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
