-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-04-2020 a las 00:29:53
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aularicardo`
--

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `barra`
-- (Véase abajo para la vista actual)
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--


-- Volcado de datos para la tabla `examen`
--

INSERT INTO `examen` (`id_examen`, `titulo`, `tiempo`, `id_profesor`, `id_asignatura`, `id_seccion`, `fecha`, `lapso`, `fechafin`) VALUES
(1, 'Herencia', '00:00:00.000000', 1, 0, 1, '2020-04-05', 0, '0000-00-00'),
(2, 'Prueba 2', '00:00:00.000000', 1, 0, 1, '2020-04-05', 0, '0000-00-00'),
(3, 'Prueba 3', '00:00:00.000000', 1, 0, 1, '2020-04-05', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `examenesprofesor`
-- (Véase abajo para la vista actual)


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fases`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id_pregunta`, `id_examen`, `id_fase`, `pregunta`, `id_respuesta`, `img`, `respuesta`) VALUES
(1, 1, 1, '¿Existe la herencia múltiple en JAVA?', 0, '', 'Ninguna de las anteriores.'),
(2, 2, 1, 'Edad de Gabriela', 0, '', '24'),
(3, 2, 1, 'nombre de la mamá de gabriela', 0, '', 'Scar'),
(4, 2, 2, 'Nombre de mi papá', 0, '', 'Roger'),
(5, 2, 3, 'Edad de Messi', 0, '', '33'),
(6, 3, 1, 'A', 0, '', '1'),
(7, 3, 2, 'b', 0, '', '1'),
(8, 3, 3, 'c', 0, '', '1');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `preguntassinresponder`
-- (Véase abajo para la vista actual)
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `realizado`
--

--
-- Volcado de datos para la tabla `realizado`
--

INSERT INTO `realizado` (`id_realizado`, `id_usuario`, `id_examen`, `nota`, `tt`, `t1`, `t2`, `t3`, `bien`, `mal`) VALUES
(1, 1, 1, 0, '00:00:16.000000', '00:00:11.000000', '00:00:03.000000', '00:00:02.000000', 0, 1),
(2, 1, 2, 0.5, '00:00:44.000000', '00:00:25.000000', '00:00:13.000000', '00:00:06.000000', 2, 2),
(3, 1, 2, 0.5, '00:01:08.000000', '00:01:06.000000', '00:00:01.000000', '00:00:01.000000', 2, 2),
(4, 1, 3, 1, '00:00:13.000000', '00:00:06.000000', '00:00:03.000000', '00:00:04.000000', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestas`
--


--
-- Volcado de datos para la tabla `repuestas`
--

INSERT INTO `repuestas` (`id_respuesta`, `id_examen`, `id_fase`, `id_pregunta`, `repuesta`, `orden`, `vf`) VALUES
(1, 1, 1, 1, 'Sí, porque todos los lenguajes de programación lo necesitan. ', 0, 0),
(2, 1, 1, 1, 'No, porque en java existe las interfaces.', 0, 0),
(3, 1, 1, 1, 'Ninguna de las anteriores.', 0, 1),
(4, 2, 1, 2, '67', 0, 0),
(5, 2, 1, 2, '24', 0, 1),
(6, 2, 1, 2, '12', 0, 0),
(7, 2, 1, 3, 'Voldemort', 0, 0),
(8, 2, 1, 3, 'Carolina', 0, 0),
(9, 2, 1, 3, 'Scar', 0, 1),
(10, 2, 2, 4, 'Voldemor ', 0, 0),
(11, 2, 2, 4, 'Ricardo', 0, 0),
(12, 2, 2, 4, 'Roger', 0, 1),
(13, 2, 3, 5, '33', 0, 1),
(14, 2, 3, 5, '34', 0, 0),
(15, 2, 3, 5, '22', 0, 0),
(16, 3, 1, 6, '1', 0, 1),
(17, 3, 1, 6, '2', 0, 0),
(18, 3, 1, 6, '3', 0, 0),
(19, 3, 2, 7, '1', 0, 1),
(20, 3, 2, 7, '2', 0, 0),
(21, 3, 2, 7, '3', 0, 0),
(22, 3, 3, 8, '1', 0, 1),
(23, 3, 3, 8, '2', 0, 0),
(24, 3, 3, 8, '3', 0, 0);

-- --------------------------------------------------------


--
-- Volcado de datos para la tabla `repuestasestudiantes`
--

INSERT INTO `repuestasestudiantes` (`id_respuestaE`, `id_pregunta`, `id_estudiante`, `id_respuesta`) VALUES
(1, 1, 1, 2),
(2, 2, 1, 5),
(3, 3, 1, 8),
(4, 4, 1, 11),
(5, 5, 1, 13),
(6, 6, 1, 16),
(7, 7, 1, 19),
(8, 8, 1, 22);

-- --------------------------------------------------------


-- Volcado de datos para la tabla `sesiones`
--

INSERT INTO `sesiones` (`id`, `nombre`) VALUES
(1, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempos`
--



--
-- Volcado de datos para la tabla `tiempos`
--

INSERT INTO `tiempos` (`id_tiempo`, `id_estudiante`, `id_examen`, `tf0`, `tf1`, `tf2`, `tf3`, `tt`) VALUES
(1, 1, 1, '19:58:00.000000', '19:58:11.000000', '19:58:14.000000', '19:58:16.000000', '00:00:00.000000'),
(2, 1, 2, '20:02:42.000000', '20:03:48.000000', '20:03:49.000000', '20:03:50.000000', '00:00:00.000000'),
(3, 1, 2, '20:03:43.000000', '20:03:48.000000', '20:03:49.000000', '20:03:50.000000', '00:00:00.000000'),
(4, 1, 3, '20:04:55.000000', '20:05:01.000000', '20:05:04.000000', '20:05:08.000000', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--



--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellido`, `fechaN`, `sexo`, `seccion`, `year`, `correo`, `direccion`, `tlf`, `id_asignatura`, `tipo`, `pass`) VALUES
(1, 'Ricardo Alberto ', 'Gonzalez Picos', '1996-09-23', 'M', '1', '2020-04-05', 'ragonzalezpicos96@gmail.com', 'Avenida Intercomunal del Valle, Residencias hipódromo bloque 2, letra B, piso 14, apartamento 4.', '04149387225', 1, 2, 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------
