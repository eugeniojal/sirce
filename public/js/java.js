$(document).ready(function() {

pagina();

 $('#ponde1').change(function() {
 
    var valor = $('#ponde1').val();
    var max = 20-valor;
    $('#ponde2').attr('max',max);
    $('#ponde3').attr('max',max);

  });
  $('#ponde2').change(function() {
 
    var valor = parseInt($('#ponde2').val());
    var valor1 = parseInt($('#ponde1').val());
    var total = valor +valor1;
    var max = 20-total;
     $('#ponde3').attr('max',max);

  });



 $('#limpiar').click(function() {
    $('input[type="file"]').val('');
  });

    $('.secciones').select2({
        placeholder: 'Selecciona una categoría',
        ajax: {
          url: '../../api/secciones.php',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });


});
function pagina(){

var pathname = window.location.pathname;
var arrayDeCadenas = pathname.split('/');

switch(arrayDeCadenas[3]){

  case 'inicio':
  $('#dash').addClass('active');
  listaExameninicio();
  estadisticasInicio();
  barrasinicio();
   calendario();
  break;

  case 'usuarios':
  $('#user').addClass('active');
  tablaUsuarios();
  break;

  case 'examenes':
  $('#exam').addClass('active');
  listaExamen();
  examenlisto();
  tablaPreguntasf1();
  tablaPreguntasf2();
  tablaPreguntasf3();
  break;
    case 'estadisticas':
  $('#report').addClass('active');
  barrasinicio();
  donaesta();
  graficatiempo();
  graficavertical();
  break;
    case 'secciones':
  $('#secc').addClass('active');
  tablaSecciones();

  break;
  case 'apoyo':
  $('#apoyo').addClass('active');


  break;
    case 'practicas':
  $('#practicas').addClass('active');
  listaExamen1();
  examenlisto1();
  tablaPreguntasf1();
  tablaPreguntasf2();
  tablaPreguntasf3();

  break;
}


}

function editaruser(id){
var url = '../../api/edituser.php';
  $.ajax({
    url:url,
    type:'POST',
    data:'id='+id,
    success:function(data){
      $('#edituser').html(data);
    }
  });

  return false;


}
function editar(){
    Swal.fire({
  title: 'Estas seguro que decea editar el usuario?',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Empezar!'
}).then((result) => {


if (result.value) {

  var id = $('#idusuario').val();
  var nombre = $('#editnombre').val();
  var apellido = $('#editapellido').val();
  var correo = $('#editcorreo').val();
  var fechan = $('#editfechan').val();
  var tlf = $('#edittlf').val();
  var pass = $('#editpass').val();
  var direccion = $('#editdirecion').val();
  var seccion = $('#editseccion').val();
  var ci = $('#editci').val();
  if (seccion == '' || seccion == null) {

  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe seleccionar una seccion!',
});

  }else{
    var url = '../../api/actualizar.php';
  $.ajax({
    url:url,
    type:'POST',
    data:{id:id,nombre:nombre,apellido:apellido,correo:correo,fechan:fechan,tlf:tlf,direccion:direccion,seccion:seccion,pass:pass,ci:ci},
    success:function(data){
      tablaUsuarios();
       $('#edit').modal('toggle');
      Swal.fire({
  icon: 'success',
  title: 'Actulizada con exito...',
  text: 'El usuario fue actualizado!',
});
  
    }
  });
  }

}

  
});
  return false;


}
function eliminarexamen(id){
    Swal.fire({
  title: 'Estas seguro que decea eliminar el examen?',
  text: "Se borraran todos los registros relacionados!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Borrar!'
}).then((result) => {
    if (result.value) {
  var url = '../../api/eliminarexamen.php';
  $.ajax({
    url:url,
    type:'POST',
    data:{id:id},
    success:function(data){
      listaExamen();
      if (data == 1) {
      Swal.fire({
  icon: 'success',
  title: 'Eliminado con exito...',
  text: 'El Examen fue Eliminado!',
});
      }else{
              Swal.fire({
  icon: 'error',
  title: 'El Examen no se puede eliminar...',
  text: 'Ya este examen fue realizado por un usuario',
});
      }

  
    }
  });
}


  
});
  return false;


}
function eliminar(id){
    Swal.fire({
  title: 'Estas seguro que decea eliminar el usuario?',
  text: "!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Empezar!'
}).then((result) => {
    if (result.value) {
  var url = '../../api/eliminar.php';
  $.ajax({
    url:url,
    type:'POST',
    data:{id:id},
    success:function(data){
      tablaUsuarios();
      if (data == 1) {
      Swal.fire({
  icon: 'success',
  title: 'Eliminado con exito...',
  text: 'El usuario fue Eliminado!',
});
      }else{
              Swal.fire({
  icon: 'error',
  title: 'El usuario no se puede eliminar...',
  text: 'El usuario no fue eliminado por que ya tiene registros',
});
      }

  
    }
  });
}


  
});
  return false;


}
function eliminarseccion(id){
    Swal.fire({
  title: 'Estas seguro que decea eliminar la sección?',
  text: "!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then((result) => {
    if (result.value) {
  var url = '../../api/eliminarseccion.php';
  $.ajax({
    url:url,
    type:'POST',
    data:{id:id},
    success:function(data){
        tablaSecciones();
      if (data == 1) {
      Swal.fire({
  icon: 'success',
  title: 'Eliminado con exito...',
  text: 'La seccion fue Eliminada!',
});
      }else{
              Swal.fire({
  icon: 'error',
  title: 'La seccion no se puede eliminar...',
  text: 'La seccion no fue eliminado por que ya tiene registros',
});
      }

  
    }
  });
}


  
});
  return false;


}
function infoexamen(id){
var url = '../../api/infoexamen.php';
  $.ajax({
    url:url,
    type:'POST',
    data:'id='+id,
    success:function(data){
      $('#infoexamen1').html(data);
    }
  });

  return false;
}
function infouser(id){
var url = '../../api/infoUser.php';
  $.ajax({
    url:url,
    type:'POST',
    data:'id='+id,
    success:function(data){
      $('#infouser').html(data);
    }
  });

  return false;
}


function GuardarFinal(){

  Swal.fire({
  title: 'Estas seguro que decea terminar la creacion del examen?',
  text: "No podra volver a modificarlo!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Terminar!'
}).then((result) => {


  if (result.value) {


     window.location='http://localhost/sirce/views/examenes/';
  }



})

}
function GuardarFinal1(){

  Swal.fire({
  title: 'Estas seguro que decea terminar la creacion del examen?',
  text: "No podra volver a modificarlo!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Terminar!'
}).then((result) => {


  if (result.value) {


     window.location='http://localhost/sirce/views/practicas/';
  }



})

}

function atras(num){
  switch(num){

  case 1:
$('#Divseccion').hide('slow');
$('#Divtitulo').show('slow');
  break;

  case 2:
$('#DivfechaF').hide('slow');
$('#Divseccion').show('slow');
  break;

  case 3:
$('#DivNotas').hide('slow');
$('#DivfechaF').show('slow');
  break;

}

}

function mostrarponde(){




$('#DivfechaF').hide('slow');
// $('#Divtitulo').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
$('#DivNotas').show('slow');


}
function mostrarSeccion(){
var titulo = $('#titulo').val();

if (titulo == '' || titulo == null) {
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe Ingresar un titulo valido !',
});
}else{


$('#Divtitulo').hide('slow');
// $('#Divtitulo').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
$('#Divseccion').show('slow');
}

}
function mostrarSeccion(){
var titulo = $('#titulo').val();

if (titulo == '' || titulo == null) {
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe Ingresar un titulo valido !',
});
}else{


$('#Divtitulo').hide('slow');
// $('#Divtitulo').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
$('#Divseccion').show('slow');
}

}



function mostrarFechas(){
var seccion = $('#secciones').val();
if (seccion == null) {
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe seleccionar una sección valida !',
});

}else{

$('#Divseccion').hide('slow');
// $('#Divtitulo').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
$('#DivfechaF').show('slow');
}

}
function tablaPreguntasf1(){
  var examen = $('#examen').val();

  $.ajax({
    url: '../../api/listaPreguntas.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: examen,f:1},
    success:function(data){
      $('#divpre').html(data);
    }
  });
}
function tablaPreguntasf2(){
  var examen = $('#examen').val();

  $.ajax({
    url: '../../api/listaPreguntas.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: examen,f:2},
    success:function(data){
      $('#divpref2').html(data);
    }
  });
}
function tablaPreguntasf3(){
  var examen = $('#examen').val();

  $.ajax({
    url: '../../api/listaPreguntas.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: examen,f:3},
    success:function(data){
      $('#divpref3').html(data);
    }
  });
}



function guardarPreguntaf1(){
  var examen = $('#examen').val();
  var Pregunta = $('#Pregunta').val();
  var respuesta1 = $('#respuesta1').val();
  var respuesta2 = $('#respuesta2').val();
  var respuesta3 = $('#respuesta3').val();
  var verdadera  = $('input:radio[name=verdadera]:checked').val();
if (verdadera == '' || verdadera == null) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe seleccionar una respuesta!',
});

}else{
if (Pregunta == '') {
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede dejar en blanco la Pregunta o Instrucción!',
});
}else{

if (respuesta1 == '' || respuesta2 == '' || respuesta3 == '') {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede dejar opciones en blanco!',
});

}else{
if (respuesta1 == respuesta2 || respuesta1 == respuesta3 || respuesta2 == respuesta3) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede existir opciones iguales!',
});
}else{
    var data=getFiles();
    data=getFormData("cargaPregunta",data);

    $.ajax({
    url: '../../api/guardarPregunta.php',
        type:"POST",
        data:data,
        dataType:"json",
        contentType:false,
        processData:false,
        cache:false,
    // data: {examen: examen,Pregunta:Pregunta,respuesta1:respuesta1,respuesta2:respuesta2,respuesta3:respuesta3,verdadera:verdadera,f:1},
    success:function(data){
      if (data==3) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Ya existe una pregunta igual en este examen!',
});
      }else{
        $("#cargaPregunta")[0].reset();
     
            tablaPreguntasf1();
            var altura = $(document).height();
             $("html, body").animate({scrollTop:altura+"px"});
      }
  
    }
  });
}

}
  
}




  }
}

function getFiles()
{
  var idFiles=document.getElementById("imagen");
  // Obtenemos el listado de archivos en un array
  var archivos=idFiles.files;
  // Creamos un objeto FormData, que nos permitira enviar un formulario
  // Este objeto, ya tiene la propiedad multipart/form-data
  var data=new FormData();
  // Recorremos todo el array de archivos y lo vamos añadiendo all
  // objeto data
  for(var i=0;i<archivos.length;i++)
  {
    // Al objeto data, le pasamos clave,valor
    data.append("archivo"+i,archivos[i]);
  }
  return data;
}
function getFormData(id,data)
{
  $("#"+id).find("input,select").each(function(i,v) {
        if(v.type!=="file") {
           if(v.type==="radio" && v.checked===true) {
                data.append(v.name,v.value);
            }else if(v.type==="radio" && v.checked===false){
               
            }else{
               data.append(v.name,v.value);
            }

        }
  });
  return data;
}
function guardarPreguntaf2(){
  var examen = $('#examen').val();
  var Pregunta = $('#Pregunta').val();
  var respuesta1 = $('#respuesta1').val();
  var respuesta2 = $('#respuesta2').val();
  var respuesta3 = $('#respuesta3').val();
  var verdadera  = $('input:radio[name=verdadera]:checked').val();
if (verdadera == '' || verdadera == null) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe seleccionar una respuesta!',
});

}else{

if (respuesta1 == '' || respuesta2 == '' || respuesta3 == '') {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede dejar opciones en blanco!',
});

}else{
if (respuesta1 == respuesta2 || respuesta1 == respuesta3 || respuesta2 == respuesta3) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede existir opciones iguales!',
});
}else{
    var data=getFiles();
    data=getFormData("cargaPregunta",data);

    $.ajax({
    url: '../../api/guardarPregunta.php',
        type:"POST",
        data:data,
        dataType:"json",
        contentType:false,
        processData:false,
        cache:false,
    // data: {examen: examen,Pregunta:Pregunta,respuesta1:respuesta1,respuesta2:respuesta2,respuesta3:respuesta3,verdadera:verdadera,f:1},
    success:function(data){
      if (data==3) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Ya existe una pregunta igual en este examen!',
});
      }else{
        $("#cargaPregunta")[0].reset();
     
            tablaPreguntasf2();
            var altura = $(document).height();
             $("html, body").animate({scrollTop:altura+"px"});
      }
  
    }
  });
}

}



  }
}
function guardarPreguntaf3(){
  var examen = $('#examen').val();
  var Pregunta = $('#Pregunta').val();
  var respuesta1 = $('#respuesta1').val();
  var respuesta2 = $('#respuesta2').val();
  var respuesta3 = $('#respuesta3').val();
  var verdadera  = $('input:radio[name=verdadera]:checked').val();
if (verdadera == '' || verdadera == null) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe seleccionar una respuesta!',
});

}else{

if (respuesta1 == '' || respuesta2 == '' || respuesta3 == '') {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede dejar opciones en blanco!',
});

}else{
if (respuesta1 == respuesta2 || respuesta1 == respuesta3 || respuesta2 == respuesta3) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'No puede existir opciones iguales!',
});
}else{
    var data=getFiles();
    data=getFormData("cargaPregunta",data);

    $.ajax({
    url: '../../api/guardarPregunta.php',
        type:"POST",
        data:data,
        dataType:"json",
        contentType:false,
        processData:false,
        cache:false,
    // data: {examen: examen,Pregunta:Pregunta,respuesta1:respuesta1,respuesta2:respuesta2,respuesta3:respuesta3,verdadera:verdadera,f:1},
    success:function(data){
      if (data==3) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Ya existe una pregunta igual en este examen!',
});
      }else{
        $("#cargaPregunta")[0].reset();
     
            tablaPreguntasf3();
            var altura = $(document).height();
             $("html, body").animate({scrollTop:altura+"px"});
      }
  
    }
  });
}

}



  }
}

function examenlisto1(){

    $('#tabExamen').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top"lf>rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/examenlisto1.php",
        "columns": [
            { "data": "Nombre" },
            { "data": "ci" },
            { "data": "titulo" },
            { "data": "nota" },
                        {"render":
        function ( data, type, row ) {
          var string = row["tt"];
          var tiempo = string.substring(0,8)
            return tiempo;

        }
  }, 
            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );




}
function examenlisto(){

    $('#tabExamen').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top"lf>rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/examenlisto.php",
        "columns": [
            { "data": "Nombre" },
            { "data": "ci" },
            { "data": "titulo" },
            { "data": "nota" },
                        {"render":
        function ( data, type, row ) {
          var string = row["tt"];
          var tiempo = string.substring(0,8)
            return tiempo;

        }
  }, 
            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );




}
function listaExamen1(){

    $('#tabUser').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top"lf>rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/listaExamen1.php",
        "columns": [
            { "data": "titulo" },
            { "data": "nombre" },
            { "data": "fecha" },
            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );




}
function listaExamen(){

    $('#tabUser').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top"lf>rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/listaExamen.php",
        "columns": [
            { "data": "titulo" },
            { "data": "nombre" },
            { "data": "fecha" },
            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );




}
function listaExameninicio(){

    $('#tabUser').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top">rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/listaExamen.php",
        "columns": [
            { "data": "titulo" },
            { "data": "nombre" },
            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );




}
function tablaUsuarios(){


    $('#tlf').mask('(####) ###-####');

    $('#tabUser').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top"lf>rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/listaUsuarios.php",
        "columns": [
              {"render":
        function ( data, type, row ) {
            return (row["nombre"] + ' ' + row["apellido"] );

        }
  }, 
            
            { "data": "ci" },
            { "data": "correo" },
            { "data": "sexo" },
            { "data": "seccion" },

            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );
}
function tablaSecciones(){


    $('#tabUser').DataTable( {
        "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
      "dom": '<"top"lf>rt<"bottom"pi><"clear">',
        
        "ajax": "../../api/ListaSecciones.php",
        "columns": [
              
            
            { "data": "id" },
            { "data": "nombre" },
            { "data": "boton" },

        ],
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    } );
}

function examen(id){

  Swal.fire({
  title: 'Estas seguro que decea empezar el examen?',
  text: "No podra volver a presentarlo!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Empezar!'
}).then((result) => {


  if (result.value) {
    // empezarDetener('Empezar');
          $.ajax({
    url: '../../api/tiempo.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen: id},
    success:function(data){
   empezarDetener('Empezar',data['tiempo'],id);

    }
  });
      $.ajax({
    url: '../../api/RegistraTiempo.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: id,fase:0},
    success:function(data){
      
    window.location='http://localhost/sirce/views/examenes/examen.php?id='+id;

    }
  });
  }
})

}

function TiempoFinal(id){
  var examen = $('#examen').val();
    $.ajax({
    url: '../../api/RegistraTiempo.php',
    type: 'POST',
    data: {examen:examen,fase:id},
    success:function(data){

    }
  });
}

function estadisticasInicio(){
  var examen = $('#examen').val();
  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'json',
    data: {examen: examen,tipo:3},
    success:function(data){
      $('#user11').html(data.estudiante);
      $('#profe').html(data.profesor);
      $('#examen').html(data.examen);
      $('#examenr').html(data.realizado);
    }
  });

}

function barrasinicio(){

 var examen = $('#examen').val();

$.ajax({
  url: '../../api/calculos.php',
  type: 'POST',
  dataType: 'JSON',
  data: {examen:examen,tipo:4},
  success:function(data1){
var mujeres = [];    
var hombres = [];
for(var i= 0; i < data1.length; i++) {
if (data1[i]['sexo'] == 'F') {
hombres.push({"label":"MUJERES","y":parseFloat(data1[i]['avg']),"color":"#E88BED"});
}else{
hombres.push({"label":"HOMBRES","y":parseFloat(data1[i]['avg']),"color":"#2B4BDC"});  
}


}
var chart = new CanvasJS.Chart("barras", {
  animationEnabled: true,
  theme: "light", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Promedio de notas"
  },
  axisY: {
    title: "Promedio de notas por genero"
  },
  data: [{        
    type: "column",  
    showInLegend: true, 

    dataPoints:hombres
  }]
});
chart.render();
    }
});

}
function toggleDataSeries(e) {
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else {
    e.dataSeries.visible = true;
  }
  chart.render();
}

function calendario(){
var examen = $('#examen').val();

  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'json',
    data: {examen:examen,tipo:5},
        success:function(data){
          var array = [];
for(var i= 0; i < data.length; i++) {

array.push({"title":data[i]['title'],"start":data[i]['start'],"end":data[i]['end']});
}
        // let datos = JSON.parse(data);
       var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var calendar = $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: true,
          selectHelper: true,
          editable: true,
          events:array
        });
    }
  });
  

 
}

function donaesta(){
var examen = $('#examen').val();
  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:6},
        success:function(data){

var array = [];
for(var i= 0; i < data.length; i++) {

array.push({"y":parseFloat(data[i]['y']),"label":data[i]['label']});
}



          
var chart = new CanvasJS.Chart("dona", {
  theme: "light2", 
  animationEnabled: true,
  title:{
    text: "Promedio por sección",
    horizontalAlign: "left"
  },
  data: [{
    type: "doughnut",
    startAngle: 60,
    //innerRadius: 60,
    indexLabelFontSize: 17,
    indexLabel: "{label} - {y}",
    toolTipContent: "<b>Nota promedio:</b> {y}",
    dataPoints: array
  }]
});
chart.render();


    }
        

  });
  
}

function graficatiempo(){
var examen = $('#examen').val();
  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:7},
        success:function(data){


var parts = data['t1'].split(':');
var t1 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data['t2'].split(':');
var t2 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data['t3'].split(':');
var t3 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);



var chart = new CanvasJS.Chart("lineas", {
  animationEnabled: true,
  theme: "light", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Promedio de tiempo por fase"
  },
  axisY: {
    title: "Promedio de tiempo por fase"
  },
  data: [{        
    type: "column",  
    showInLegend: true, 
    indexLabel: "{y}.Seg",
    toolTipContent: "<b>Tiempo promedio:</b> {y}.Seg",
  dataPoints: [      
            { y: t1, label: "FASE 1" },
            { y: t2,  label: "FASE 2" },
            { y: t3,  label: "FASE 3" },
        ]

  }]
});
chart.render();



    }
        

  });
  
}

function graficavertical(){
var examen = $('#examen').val();
  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:8},
        success:function(data){

var array = [];
for(var i= 0; i < data.length; i++) {

array.push({"y":parseFloat(data[i]['y']),"label":data[i]['label'],"indexLabel":data[i]['indexLabel']});
}
var options = {
  animationEnabled: true,
  title: {
    text: "Promedios por Examen",                
    fontColor: "Peru"
  },  
  axisY: {
    tickThickness: 0,
    lineThickness: 0,
    valueFormatString: " ",
    gridThickness: 0                    
  },
  axisX: {
    tickThickness: 0,
    lineThickness: 0,
    labelFontSize: 18,
    labelFontColor: "Peru"        
  },
  data: [{
    indexLabelFontSize: 10,
    toolTipContent: "<span style=\"color:#62C9C3\">{indexLabel}:</span> <span style=\"color:#CD853F\"><strong>{y}</strong></span>",
    indexLabelPlacement: "inside",
    indexLabelFontColor: "black",
    indexLabelFontWeight: 600,
    indexLabelFontFamily: "Verdana",
    color: "#62C9C3",
    type: "bar",
    dataPoints: array
  }]
};

$("#vertical").CanvasJSChart(options);

    }
        

  });
  
}