$(document).ready(function() {


mostrarPreguntas();


piechart();
nota();
  
  var examen = $('#examen').val();
  $.ajax({
    url: '../../api/listamala.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: examen},
    success:function(data){
      $('#listamala').html(data);

    }
  });




});
function nota(){
  var examen = $('#examen').val();
  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'json',
    data: {examen: examen,tipo:2},
    success:function(data){
      $('#buenas').html(data.bien);
      $('#malas').html(data.mal);
      $('#nota').html(data.nota);
    }
  });

}
function piechart(){
var examen = $('#examen').val();
$.ajax({
  url: '../../api/calculos.php',
  type: 'POST',
  dataType: 'json',
  data: {examen:examen,tipo:1},
  success:function(data1){
     var chart = new CanvasJS.Chart("chartContainer", {
  exportEnabled: true,
  animationEnabled: true,
  title:{
    text: "Tiempo en cada fase"
  },
  legend:{
    cursor: "pointer",
    itemclick: explodePie
  },
  data: [{
    type: "pie",
    showInLegend: true,
    toolTipContent: "{name}: <strong>{y} .Seg</strong>",
    legendText: "{name}",
       dataPoints: [
      { y: parseInt(data1.t1), name: "Fase 1", exploded: true },
      { y: parseInt(data1.t2), name: "Fase 2",exploded: true },
      { y: parseInt(data1.t3), name: "Fase 3" ,exploded: true},
    ]
    
  }]
});
chart.render();
    }
});



}




function explodePie (e) {
  if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
    e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
  } else {
    e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
  }
  e.chart.render();

}
function mostrarPreguntas(fase){

  var examen = $('#examen').val();
  var fase = $('#fase').val();

  $.ajax({
    url: '../../api/examen.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: examen,f:fase},
    success:function(data){
       funcionando();
      $('#divExamen').html(data);
    }
  });

}
function siguiente(id){
 var respuesta  = $('input:radio[name=radiobtns]:checked').val();
 if (respuesta == '' || respuesta == null) {
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Debe seleccionar una respuesta!',
});

}else{
    $.ajax({
    url: '../../api/respuesta.php',
    type: 'POST',
    dataType: 'html',
    data: {respuesta: respuesta,pregunta:id},
    success:function(data){
     mostrarPreguntas();
    }
  });
}
}
function siguientefase(id){
  var examen = $('#examen').val();
  $.ajax({
    url: '../../api/RegistraTiempo.php',
    type: 'POST',
    data: {examen:examen,fase:id},
    success:function(data){
var i = Number(data); 
      switch(i){

  case 2:
window.location='http://localhost/sirce/views/examenes/examen2.php?id='+examen;
  break;

  case 3:
window.location='http://localhost/sirce/views/examenes/examen3.php?id='+examen;
  break;

  case 4:
window.location='http://localhost/sirce/views/examenes/';
  break;

}


    }
  });
}

function culminar(){
  var examen = $('#examen').val();
  TiempoFinal(3);
  empezarDetener('Detener',0);
  $.ajax({
    url: '../../api/culminar.php',
    type: 'POST',
    data: {examen:examen},
    success:function(data){
     window.location='http://localhost/sirce/views/examenes/resultados.php?id='+examen;
    }
  });
}

function eliminarpre(id,fase){
    Swal.fire({
  title: 'Estas seguro que decea eliminar la pregunta?',
  text: "!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Empezar!'
}).then((result) => {
    if (result.value) {
  var url = '../../api/eliminarpre.php';
  $.ajax({
    url:url,
    type:'POST',
    data:{id:id},
    success:function(data){
      tablaUsuarios();
      if (data == 1) {
      Swal.fire({
  icon: 'success',
  title: 'Eliminado con exito...',
  text: 'La pregunta fue Eliminada!',
});
      }

      switch(fase){
        case 1:
        tablaPreguntasf1();
        break;
        case 2:

        tablaPreguntasf2();
        break;
        case 3:
        tablaPreguntasf3();
        break;
      }

  
    }
  });
}


  
});
  return false;


}