<?php include('../header.php'); ?>

<div class="main">

<div class="main-inner">

<div class="container">

<div class="row">

<div class="span12">

<div class="widget ">

<div class="widget-header">
<i class="icon-user"></i>
<h3>Usuarios</h3>
</div>
<!-- /widget-header -->

<div class="widget-content">

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li>
            <a href="#formcontrols" data-toggle="tab">Registrar usuario</a>
        </li>
        <li class="active"><a href="#verUsers" data-toggle="tab">Ver Usuarios</a></li>
        <!-- <li><a href="#jscontrols" data-toggle="tab">Regitrar sección</a></li> -->
    </ul>

    <br>

    <div class="tab-content">
        <div class="tab-pane " id="formcontrols">
            <form id="edit-profile" class="form-horizontal" action="<?php echo API; ?>RegistroUsuario.php" method="POST">
                <fieldset>
                    <?php if (isset($_REQUEST['q'])): ?>
                        <?php if ($_REQUEST['q'] == 1): ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Aviso!</strong> Usuario creado con exito!.
                            </div>
                            <?php endif ?>
                                <?php if ($_REQUEST['q'] == 2): ?>
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Aviso!</strong>
                                        <?php echo $_REQUEST['mensaje']; ?>.
                                    </div>
                                    <?php endif ?>

                                        <?php endif ?>

                                            <div class="control-group">
                                                <label class="control-label" for="firstname">Nombre</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" name="nombre" id="firstname" value="John">
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="lastname">Apellido</label>
                                                <div class="controls">
                                                    <input type="text" name="apellido" class="span6" id="lastname" value="Donga">
                                                </div>
                                                <!-- /controls -->
                                            </div>

                                             <div class="control-group">
                                                <label class="control-label" for="cedula">cedula</label>
                                                <div class="controls">
                                                    <input type="text" name="cedula" class="span6" id="cedula" value="V-1000000">
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="email">Email Address</label>
                                                <div class="controls">
                                                    <input type="email" class="span4" name="email" id="email" value="john.donga@egrappler.com">
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <br />
                                            <br />

                                            <div class="control-group">
                                                <label class="control-label" for="password1">Password</label>
                                                <div class="controls">
                                                    <input type="password" class="span4" id="password1" name="pass">
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="password2">Confirm</label>
                                                <div class="controls">
                                                    <input type="password" class="span4" id="password2">
                                                </div>
                                                <!-- /controls -->
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Tipo de usuario</label>
                                                <?php if ($_SESSION['nivel'] == 3): ?>
                                                       <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" value="1" name="radiobtns" > Estudiante
                                                    </label>

                                                    <label class="radio inline">
                                                        <input type="radio" value="2" name="radiobtns"> Profesor
                                                    </label>
                                                    <label class="radio inline">
                                                        <input type="radio" value="3" name="radiobtns"> Administrador
                                                    </label>
                                                </div>
                                                <?php else: ?>
                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" value="1" name="radiobtns" checked="true"> Estudiante
                                                    </label>
                                                </div>
                                                <?php endif ?>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->
                                            <div class="control-group">
                                                <label class="control-label">Sexo</label>

                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" value="M" name="sexo"> Masculino
                                                    </label>

                                                    <label class="radio inline">
                                                        <input type="radio" value="F" name="sexo"> Femenino
                                                    </label>
                                                </div>
                                                <!-- /controls -->
                                            </div>

                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="radiobtns">sección</label>

                                                <div class="controls">
                                                    <div class="input-prepend input-append">

                                                        <select class="secciones form-control" name="secciones" required></select>

                                                    </div>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->
                                            <div class="control-group">
                                                <label class="control-label" for="fechan">Fecha Nacimiento</label>

                                                <div class="controls">
                                                    <div class="input-prepend input-append">

                                                        <input type="date" name="fechan" value="" id="fechan" placeholder="18/12/1997">

                                                    </div>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->
                                            <div class="control-group">
                                                <label class="control-label" for="tlf">Telefono</label>

                                                <div class="controls">
                                                    <div class="input-prepend input-append">
                                                        <input type="text" class="tlf" name="tlf" id="tlf" placeholder="(0414)366-3615" maxlength="15">

                                                    </div>
                                                </div>
                                                <!-- /controls -->
                                            </div>

                                            <!-- /control-group -->
                                            <div class="control-group">
                                                <label class="control-label" for="radiobtns">Dirección</label>

                                                <div class="controls">
                                                    <div class="input-prepend input-append">

                                                        <textarea name="direccion" rows="5" class="span6"></textarea>

                                                    </div>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <br />

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary">Guardar</button>
                                              
                                            </div>
                                            <!-- /form-actions -->
                </fieldset>
            </form>
        </div>
        <!-- PANTALLA DE REGISTRO  -->

        <div class="tab-pane active" id="verUsers">

            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Lista de usuarios </h3>
            </div>
            <div class="widget-content">
                <table id="tabUser" class=" table table-striped table-bordered " style="text-align: center;" width="100%">
                    <thead>
                        <tr>
                            <th> Nombre y Apellido</th>
                            <th> Documento</th>
                            <th> correo</th>
                            <th> sexo</th>
                            <th> Sección</th>
                            <th class="td-actions">opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th> Nombre y Apellido</th>
                            <th> Documento</th>
                            <th> correo</th>
                            <th> sexo</th>
                            <th> Sección</th>
                            <th class="td-actions">opciones</th>
                        </tr>

                    </tfoot>
                </table>
            </div>

            <div id="verUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Información de usuario</h3>
                </div>
                <div class="modal-body">
                    <center>
                    <img src="<?php echo RUTA; ?>img/user.jpg" class="rounded mx-auto d-block" alt="usuario" width="200px">
                    <div id="infouser" style="text-align: left;padding-left:100px;padding-top:10px;" >
                        
                    </div>    


                    </center>

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                
                </div>
            </div>

        </div>
        <!-- PANTALLA DE VER USUARIOS -->
        <div class="tab-pane " id="jscontrols">
            <form id="edit-profile2" class="form-vertical">
                <fieldset>

                    <div class="control-group">
                        <label class="control-label">Alerts</label>
                        <div class="controls">
                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Warning!</strong> Best check yo self, you're not looking too good.
                            </div>

                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Warning!</strong> Best check yo self, you're not looking too good.
                            </div>

                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Warning!</strong> Best check yo self, you're not looking too good.
                            </div>

                            <div class="alert alert-block">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <h4>Warning!</h4> Best check yo self, you're not...
                            </div>
                        </div>
                        <!-- /controls -->

                    </div>
                    <!-- /control-group -->

                    <div class="control-group">
                        <label class="control-label">Progress Bar</label>
                        <div class="controls">
                            <div class="progress">
                                <div class="bar" style="width: 60%;"></div>
                            </div>

                            <div class="progress progress-striped">
                                <div class="bar" style="width: 20%;"></div>
                            </div>

                            <div class="progress progress-striped active">
                                <div class="bar" style="width: 40%;"></div>
                            </div>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                    <div class="control-group">
                        <label class="control-label">Accordion</label>
                        <div class="controls">

                            <div class="accordion" id="accordion2">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            Collapsible Group Item #1
                          </a>
                                    </div>
                                    <div id="collapseOne" class="accordion-body collapse in">
                                        <div class="accordion-inner">
                                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            Collapsible Group Item #2
                          </a>
                                    </div>
                                    <div id="collapseTwo" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anim pariatur cliche...
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                    <div class="control-group">
                        <label class="control-label">Progress Bar</label>
                        <div class="controls">
                            <!-- Button to trigger modal -->
                            <a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>

                            <!-- Modal -->
                            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Thank you for visiting EGrappler.com</h3>
                                </div>
                                <div class="modal-body">
                                    <p>One fine body…</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                    <button class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                    <div class="control-group">
                        <label class="control-label">Social Buttons</label>
                        <div class="controls">
                            <button class="btn btn-facebook-alt"><i class="icon-facebook-sign"></i> Facebook</button>
                            <button class="btn btn-twitter-alt"><i class="icon-twitter-sign"></i> Twitter</button>
                            <button class="btn btn-google-alt"><i class="icon-google-plus-sign"></i> Google+</button>
                            <button class="btn btn-linkedin-alt"><i class="icon-linkedin-sign"></i> Linked In</button>
                            <button class="btn btn-pinterest-alt"><i class="icon-pinterest-sign"></i> Pinterest</button>
                            <button class="btn btn-github-alt"><i class="icon-github-sign"></i> Github</button>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->

                    <br />

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button class="btn">Cancel</button>
                        <button class="btn btn-info">Info</button>
                        <button class="btn btn-danger">Danger</button>
                        <button class="btn btn-warning">Warning</button>
                        <button class="btn btn-invert">Invert</button>
                        <button class="btn btn-success">Success</button>
                    </div>
                </fieldset>
            </form>
        </div>

    </div>

</div>

</div>
<!-- /widget-content -->

</div>
<!-- /widget -->

</div>
<!-- /span8 -->

</div>
<!-- /row -->

</div>
<!-- /container -->

</div>
<!-- /main-inner -->

</div>
            <div id="edit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal=content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Información de usuario</h3>
                </div>

                <div class="modal-body">
                    <center>
                    <img src="<?php echo RUTA; ?>img/user.jpg" class="rounded mx-auto d-block" alt="usuario" width="200px">
                    <div id="edituser" style="text-align: left;padding-left:0px;padding-top:10px;" >
                        
                    </div>    


                    </center>
<li style="margin-bottom: 10px;" >SECCIÓN:<select class="secciones form-control" id="editseccion" name="secciones" ></select></li>
                </div>
                <div class="modal-footer">
                    <a onclick="editar();" class="btn btn-primary">Editar</a>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                
                </div>
                </div>
                </div>
            </div>
<!-- /main -->
<?php include('../footer.php'); ?>