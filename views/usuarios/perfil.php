<?php include('../header.php'); ?>

    <div class="main">

        <div class="main-inner">

            <div class="container">

                <div class="row">

                    <div class="span12">

                        <div class="widget ">

            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Perfil</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">

                  <h6 class="bigstats">Datos personales</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-user"></i> <span class="" id=""></span><br><?php echo $_SESSION['nombre'];?><br></div>
                    <!-- .stat -->
                    
                
                  </div>
                    

                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>

                        </div>
                        <!-- /widget -->

                    </div>
                    <div class="span6">
                
                <div id="target-2" class="widget">
                    
                    <div class="widget-content">
                        
                        <h1>Datos de contacto</h1>
                        
                        <ul>
                            <li id="seccion"></li>
                            <li id="Fecha"></li>
                            <li id="sexo"></li>
                            <li id="telefono"></li>
                            <li id="correo"></li>
                        </ul>
                        
                    </div> <!-- /widget-content -->
                    
                </div> <!-- /widget -->
                
            </div>
            <div class="span6">
                
                <div id="target-2" class="widget">
                    
                    <div class="widget-content">
                        
                        <h1>Dirección</h1>
                        
                        <ul>
                            <li style="padding: 5px;" id="direccion"></li>

                        </ul>
                        
                    </div> <!-- /widget-content -->
                    
                </div> <!-- /widget -->
                
            </div>
                    <!-- /span8 -->

                </div>
                <!-- /row -->

<?php if ($_SESSION['nivel'] == 1): ?>

    <input type="hidden" id="usuario" name="" value="<?php echo $_SESSION['user_id']; ?>">

                    <div class="row">
                    <div class="span12">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content" >

                              <center>
                                  
                                  <div class="stats-box-title">Promedio general </div>
                    <i class="icon-edit" style="color:#3C3" id="nota"></i>
                    <div id="promedio"></div>
                              </center>
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                                 <div id="lineas" style="height: 250px; width: 100%;"></div>
                              
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Promedio por examen</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                              <div id="dona" style="height: 250px; width: 100%;"></div>
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>

                    <!-- /span6 -->
                </div>







<?php endif ?>
















            </div>
            <!-- /container -->

        </div>
        <!-- /main-inner -->

    </div>
    <!-- /main -->
    <?php include('../footer.php'); ?>
        <script type="text/javascript">
      $(document).ready(function(){
        $.getJSON("../../api/perfil.php",function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                    $("li#seccion").append('Sección: '+V['nombre']);
                    $("li#Fecha").append('Fecha: '+V['fechaN']);
                    $("li#sexo").append('Sexo: '+V['sexo']);
                    $("li#telefono").append('Telefono #: '+V['tlf']);
                    $("li#correo").append('Correo #: '+V['correo']);
                    $("li#direccion").append(V['direccion']);
                });
            }
        });


         var examen = $('#usuario').val();


//   $.ajax({
//     url: '../../api/calculos.php',
//     type: 'POST',
//     dataType: 'JSON',
//     data: {examen:examen,tipo:12},
//         success:function(data){

// var parts = data['t1'].split(':');
// var t1 = parseInt(parts[0]) * 60 + parseInt(parts[1]);
// var parts = data['t2'].split(':');
// var t2 = parseInt(parts[0]) * 60 + parseInt(parts[1]);
// var parts = data['t3'].split(':');
// var t3 = parseInt(parts[0]) * 60 + parseInt(parts[1]);

// var chart = new CanvasJS.Chart("lineas", {
//   animationEnabled: true,
//   theme: "light2",
//   title:{
//     text: "promedio de tiempo en cada Fase "
//   },
//     toolTip: {
//     shared: true
//   },
//   axisX: {
//     title: "Fases",
//     suffix : " FASE",
//     minimum: 0,
//   },
//   axisY: {
//     title: "Minutos",
//   },
//   toolTipContent: "<b>{label}:</b>{y:}.min",
//   data: [{       
//     type: "spline",
//     yValueFormatString: "####..min",
//     xValueFormatString: "FASE #",
//     name: "Tiempo de fases",
//     dataPoints:[
//       { y: 0,x:0},
//       { y: t1,x:1},
//       { y: t2,x:2},
//       { y: t3,x:3}
//     ]
//   }]
// });
// chart.render();


//     }
        

//   });
  


  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:13},
        success:function(data){

var array = [];
for(var i= 0; i < data.length; i++) {

array.push({"y":parseFloat(data[i]['y']),"label":data[i]['label']});
}
          
var chart = new CanvasJS.Chart("dona", {
  theme: "light2", 
  animationEnabled: true,
  title:{
    text: "Promedio por examen",
    horizontalAlign: "left"
  },
  data: [{
    type: "doughnut",
    startAngle: 60,
    //innerRadius: 60,
    indexLabelFontSize: 17,
    indexLabel: "{label} - promedio:{y}",
    toolTipContent: "<b>Nota promedio:</b> {y}",
    dataPoints: array
  }]
});
chart.render();


    }
        

  });
        $.getJSON("../../api/promedio.php",{examen:examen,tipo:2},function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                    $("#promedio").append(V['promedio']);

                });
            }
        });



$.ajax({
  url: '../../api/calculos.php',
  type: 'POST',
  dataType: 'JSON',
  data: {examen:examen,tipo:12},
  success:function(data1){



var parts = data1['t1'].split(':');
var t1 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data1['t2'].split(':');
var t2 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data1['t3'].split(':');
var t3 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);



var chart = new CanvasJS.Chart("lineas", {
  animationEnabled: true,
  theme: "light", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Promedio de tiempo por fase"
  },
  axisY: {
    title: "Promedio de tiempo por fase"
  },
  data: [{        
    type: "column",  
    showInLegend: true, 
    indexLabel: "{y}.Seg",
    toolTipContent: "<b>Tiempo promedio:</b> {y}.Seg",
  dataPoints: [      
            { y: t1, label: "FASE 1" },
            { y: t2,  label: "FASE 2" },
            { y: t3,  label: "FASE 3" },
        ]

  }]
});
chart.render();
    }
});






      });
    </script>
