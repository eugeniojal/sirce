<?php 

include('../header.php');
?>
<input type="hidden" id="usuario" name="" value="<?php echo $_GET['usuario']; ?>">
    <!-- /subnavbar -->
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content" >

                              <center>
                                  
                                  <div class="stats-box-title">Promedio general </div>
                    <i class="icon-edit" style="color:#3C3" id="nota"></i>
                    <div id="promedio"></div>
                              </center>
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                                 <div id="lineas" style="height: 250px; width: 100%;"></div>
                              
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Promedio por examen</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                              <div id="dona" style="height: 250px; width: 100%;"></div>
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>

                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
<?php
include('../footer.php');
 ?>

<script type="text/javascript">
      $(document).ready(function(){

 var examen = $('#usuario').val();


  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:12},
        success:function(data){



var parts = data['t1'].split(':');
var t1 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data['t2'].split(':');
var t2 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data['t3'].split(':');
var t3 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);



var chart = new CanvasJS.Chart("lineas", {
  animationEnabled: true,
  theme: "light", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Promedio de tiempo por fase"
  },
  axisY: {
    title: "Promedio de tiempo por fase"
  },
  data: [{        
    type: "column",  
    showInLegend: true, 
    indexLabel: "{y}.Seg",
    toolTipContent: "<b>Tiempo promedio:</b> {y}.Seg",
  dataPoints: [      
            { y: t1, label: "FASE 1" },
            { y: t2,  label: "FASE 2" },
            { y: t3,  label: "FASE 3" },
        ]

  }]
});
chart.render();


    }
        

  });
  


  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:13},
        success:function(data){

var array = [];
for(var i= 0; i < data.length; i++) {

array.push({"y":parseFloat(data[i]['y']),"label":data[i]['label']});
}
          
var chart = new CanvasJS.Chart("dona", {
  theme: "light2", 
  animationEnabled: true,
  title:{
    text: "Promedio por examen",
    horizontalAlign: "left"
  },
  data: [{
    type: "doughnut",
    startAngle: 60,
    //innerRadius: 60,
    indexLabelFontSize: 17,
    indexLabel: "{label} - promedio:{y}",
    toolTipContent: "<b>Nota promedio:</b> {y}",
    dataPoints: array
  }]
});
chart.render();


    }
        

  });
        $.getJSON("../../api/promedio.php",{examen:examen,tipo:2},function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                    $("#promedio").append(V['promedio']);

                });
            }
        });
      });
    </script>