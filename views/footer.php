<!-- <div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2020 <a href="#">Corporacion CEG. C.A</a>. </div>

      </div>

    </div>

  </div>
</div> -->

<script src="<?php echo RUTA; ?>js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo RUTA; ?>js/excanvas.min.js"></script> 
<!-- <script src="<?php echo RUTA; ?>js/chart.min.js" type="text/javascript"></script>  -->
<script src="<?php echo RUTA; ?>js/bootstrap.js"></script>
<script src="<?php echo RUTA; ?>js/select2.min.js"></script>
<script src="<?php echo RUTA; ?>js/jquery.mask.min.js"></script>
<script src="<?php echo RUTA; ?>js/all.js"></script>
<script src="<?php echo RUTA; ?>js/sweetalert2.all.js"></script>
<script src="<?php echo RUTA; ?>js/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo RUTA; ?>js/full-calendar/fullcalendar.min.js"></script>
 
<script src="<?php echo RUTA; ?>js/base.js"></script> 
<script src="<?php echo RUTA; ?>js/java.js"></script> 
<script src="<?php echo RUTA; ?>js/examen.js"></script> 
<script src="<?php echo RUTA; ?>js/jquery.canvasjs.min.js"></script> 
    <script>
var inicio=0;
    var timeout=0;
 
    function empezarDetener(elemento,int,id)
    {
        examen=localStorage.getItem("examen");
        if(timeout==0)
        {
            // empezar el cronometro
            if (id != examen ) {

            elemento.value="Detener";
            // Obtenemos el valor actual
            inicio=new Date().getTime();
            // Guardamos el valor inicial en la base de datos del navegador
            localStorage.setItem("inicio",inicio);
            var i = int.split(':');
            var theBigDay = new Date();
            theBigDay.setHours(parseInt(i[0]),parseInt(i[1]),parseInt(i[2]));
            var fin = theBigDay.getTime();
            localStorage.setItem("fin",fin);
            localStorage.setItem("examen",id);
            }
            
            // iniciamos el proceso
            funcionando();


        }else{
            // detemer el cronometro
 
            elemento.value="Empezar";
            clearTimeout(timeout);
 
            // Eliminamos el valor inicial guardado
            localStorage.removeItem("inicio");
            localStorage.removeItem("fin");
            localStorage.removeItem("examen");
            timeout=0;
        }
    }
 
    function funcionando()
    {
        // obteneos la fecha actual
        var actual = new Date().getTime();
        fin=localStorage.getItem("fin");
        // obtenemos la diferencia entre la fecha actual y la de inicio
        var diff1=new Date(actual-inicio);
        var diff=new Date(fin-diff1);
 
        // mostramos la diferencia entre la fecha actual y la inicial
        var result=LeadingZero(diff.getHours())+":"+LeadingZero(diff.getMinutes())+":"+LeadingZero(diff.getSeconds());
        document.getElementById('crono').innerHTML = result;

        if (result == '00:10:00') {

        document.getElementById('crono').style.color = 'red' ;

        }
        if (result == '00:00:00') {
            culminar();
        }
        // Indicamos que se ejecute esta función nuevamente dentro de 1 segundo
        timeout=setTimeout("funcionando()",1000);
    }
 
    /* Funcion que pone un 0 delante de un valor si es necesario */
    function LeadingZero(Time)
    {
        return (Time < 10) ? "0" + Time : + Time;
    }
 
    window.onload=function()
    {
        if(localStorage.getItem("inicio")!=null)
        {
            // Si al iniciar el navegador, la variable inicio que se guarda
            // en la base de datos del navegador tiene valor, cargamos el valor
            // y iniciamos el proceso.
            inicio=localStorage.getItem("inicio");
            // document.getElementById("boton").value="Detener";
            funcionando();
        }
    }
    </script>

    </body>
</html>