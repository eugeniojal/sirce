<?php include('../header.php'); ?>

<div class="main">

<div class="main-inner">

<div class="container">

<div class="row">

<div class="span12">

<div class="widget ">

<div class="widget-header">
<i class="icon-user"></i>
<h3>Secciones</h3>
</div>
<!-- /widget-header -->

<div class="widget-content">

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li>
            <a href="#formcontrols" data-toggle="tab">Registrar Sección</a>
        </li>
        <li class="active"><a href="#verUsers" data-toggle="tab">Ver Secciones</a></li>
        <!-- <li><a href="#jscontrols" data-toggle="tab">Regitrar sección</a></li> -->
    </ul>

    <br>

    <div class="tab-content">
        <div class="tab-pane " id="formcontrols">
            <form id="edit-profile" class="form-horizontal" action="<?php echo API; ?>RegistroSeccion.php" method="POST">
                <fieldset>
                    <?php if (isset($_REQUEST['q'])): ?>
                        <?php if ($_REQUEST['q'] == 1): ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Aviso!</strong> Sección creado con exito!.
                            </div>
                            <?php endif ?>
                                <?php if ($_REQUEST['q'] == 2): ?>
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Aviso!</strong>
                                        <?php echo $_REQUEST['mensaje']; ?>.
                                    </div>
                                    <?php endif ?>

                                        <?php endif ?>

                                            <div class="control-group">
                                                <label class="control-label" for="firstname">Nombre</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" name="nombre" id="firstname" value="Sección A">
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->




                                            <!-- /control-group -->



                                            <!-- /control-group -->

    

                                            <!-- /control-group -->


                                            <!-- /control-group -->


                                            <!-- /control-group -->

                                            <!-- /control-group -->

                                            <!-- /control-group -->

                                            <br />

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary">Guardar</button>
                                              
                                            </div>
                                            <!-- /form-actions -->
                </fieldset>
            </form>
        </div>
        <!-- PANTALLA DE REGISTRO  -->

        <div class="tab-pane active" id="verUsers">

            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Lista de secciones </h3>
            </div>
            <div class="widget-content">
                <table id="tabUser" class=" table table-striped table-bordered " style="text-align: center;" width="100%">
                    <thead>
                        <tr>
                            <th> N#</th>
                            <th> Sección</th>
                            <th class="td-actions">opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th> N#</th>
                            <th> Sección</th>
                            <th class="td-actions">opciones</th>
                        </tr>

                    </tfoot>
                </table>
            </div>

            <div id="verUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Información de usuario</h3>
                </div>
                <div class="modal-body">
                    <center>
                    <img src="<?php echo RUTA; ?>img/user.jpg" class="rounded mx-auto d-block" alt="usuario" width="200px">
                    <div id="infouser" style="text-align: left;padding-left:100px;padding-top:10px;" >
                        
                    </div>    


                    </center>

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                
                </div>
            </div>

        </div>
        <!-- PANTALLA DE VER USUARIOS -->


    </div>

</div>

</div>
<!-- /widget-content -->

</div>
<!-- /widget -->

</div>
<!-- /span8 -->

</div>
<!-- /row -->

</div>
<!-- /container -->

</div>
<!-- /main-inner -->

</div>
            <div id="edit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog " role="document">
                <div class="modal=content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Información de usuario</h3>
                </div>

                <div class="modal-body">
                    <center>
                    <img src="<?php echo RUTA; ?>img/user.jpg" class="rounded mx-auto d-block" alt="usuario" width="200px">
                    <div id="edituser" style="text-align: left;padding-left:0px;padding-top:10px;" >
                        
                    </div>    


                    </center>
<li style="margin-bottom: 10px;" >SECCIÓN:<select class="secciones form-control" id="editseccion" name="secciones" ></select></li>
                </div>
                <div class="modal-footer">
                    <a onclick="editar();" class="btn btn-primary">Editar</a>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                
                </div>
                </div>
                </div>
            </div>
<!-- /main -->
<?php include('../footer.php'); ?>