<?php include('../header.php'); ?>
<link rel="stylesheet" href="<?php echo RUTA; ?>css/reports.css">
<input type="hidden" id="examen" name="" value="<?php echo $_GET['examen']; ?>">
<input type="hidden" id="usuario" name="" value="<?php echo $_GET['user']; ?>">
<!-- <input type="hidden" id="fase" name="" value="2"> -->
<div class="main">
    
    <div class="main-inner">

        <div class="container">
            
         <div class="row">
            
            <div class="span12">
          
            <div class="info-box">
               <div class="row-fluid stats-box">
                  <div class="span4">
                    <div class="stats-box-title">Respuestas Correctas</div>
                    <div class="stats-box-all-info"><i class="icon-ok" style="color:#3366cc;"  id="buenas"></i></div>
                    <!-- <div class="wrap-chart"><div id="visitor-stat" class="chart" style="padding: 0px; position: relative;"><canvas id="bar-chart1" class="chart-holder" height="150" width="325"></canvas></div></div> -->
                  </div>
                  
                  <div class="span4">
                    <div class="stats-box-title">Incorrectas</div>
                    <div class="stats-box-all-info"><i class="icon-remove"  style="color:#F30" id="malas"></i> <div ></div></div>
<!--                     <div class="wrap-chart"><div id="order-stat" class="chart" style="padding: 0px; position: relative;"><canvas id="bar-chart2" class="chart-holder" height="150" width="325"></canvas></div></div> -->
                  </div>
                  
                  <div class="span4">
                    <div class="stats-box-title">Nota Final</div>
                    <div class="stats-box-all-info"><i class="icon-edit" style="color:#3C3" id="nota"></i> <div ></div></div>
                    <div class="wrap-chart">
<!--                     
                    <div id="user-stat" class="chart" style="padding: 0px; position: relative;"><canvas id="bar-chart3" class="chart-holder" height="150" width="325"></canvas></div> -->
                    </div>
                  </div>
<!--                   <div id="user-stat" class="chart" style="padding: 0px; position: relative;"><canvas id="bar-chart3" class="chart-holder" height="150" width="325"></canvas>
               </div> -->
               
             </div>
            
         </div>
         </div>     
         </div> 
            
          <!-- /row -->
    
          <div class="row">
            
            <div class="span12">
                
                <div class="widget">
                        
                    <div class="widget-header">
                        <i class="icon-star"></i>
                        <h3>GRAFICA DE TIEMPO</h3>
                    </div> <!-- /widget-header -->
                    
                    <div class="widget-content">
                       
                <div id="chartContainer"  class="chart-holder" height="300" width="508"></div>
                    </div> <!-- /widget-content -->
                        
                </div> <!-- /widget -->
                
                
                
                
            </div> <!-- /span6 -->
            
            
 <!--            <div class="span6">
                
                <div class="widget">
                            
                    <div class="widget-header">
                        <i class="icon-list-alt"></i>
                        <h3>Another Chart</h3>
                    </div>
                    
                    <div class="widget-content">
                        <canvas id="bar-chart" class="chart-holder" height="250" width="538"></canvas>
                    </div> 
                
                </div> 
                                    
              </div> 
             -->
          </div> <!-- /row -->


          <div class="row">
            <div class="span12">
                <div class="widget">
                    <div class="widget-header">
                        <i class="icon-star"></i>
                        <h3></h3>
                    </div> <!-- /widget-header -->
                    <div class="widget-content">
                       <center>
                         <table class="table table-bordered" style="text-align: center !important;">
                           <caption>Respuestas erroneas</caption>
                           <thead>
                             <tr>
                               <th>Fase</th>
                               <th>preguntas o instrucciones</th>
                               <th>Respuesta Correcta</th>
                               <th>Tu respuesta</th>
                             </tr>
                           </thead>
                           <tbody id="listamala">
                           </tbody>
                         </table>
                       </center>
                    </div> <!-- /widget-content -->  
                </div> <!-- /widget --> 
            </div>
          </div>


          <div class="row">
            <div class="span12">
                <div class="widget">  
                    <div class="widget-header">
                        <i class="icon-star"></i>
                        <h3></h3>
                    </div> 
                    <div class="widget-content">
                       <center>
                          <a href="index.php" class="btn btn-primary" style="width:200px;height: 50px;"><br>salir</a>
                       </center>
                    </div> 
                </div> 
            </div>
          </div>



          
          
            
          
          
        </div> <!-- /container -->
        
    </div> <!-- /main-inner -->
    
</div> <!-- /main -->
    <!-- /main -->
    <?php include('../footer.php'); ?>


            <script type="text/javascript">
      $(document).ready(function(){


  var examen = $('#examen').val();
  var usuario = $('#usuario').val();
  $.ajax({
    url: '../../api/listamala1.php',
    type: 'POST',
    dataType: 'html',
    data: {examen: examen,usuario:usuario},
    success:function(data){
      $('#listamala').html(data);

    }
  });
  $.ajax({
    url: '../../api/notas.php',
    type: 'POST',
    dataType: 'json',
    data: {examen: examen,usuario:usuario,tipo:2},
    success:function(data){
        $('#buenas').html(data.bien);
      $('#malas').html(data.mal);
      $('#nota').html(data.nota);

    }
  });

 $.ajax({
  url: '../../api/notas.php',
  type: 'POST',
  dataType: 'json',
  data: {examen:examen,usuario:usuario,tipo:1},
  success:function(data1){
     var chart = new CanvasJS.Chart("chartContainer", {
  exportEnabled: true,
  animationEnabled: true,
  title:{
    text: "Tiempo en cada fase"
  },
  legend:{
    cursor: "pointer",
    itemclick: explodePie
  },
  data: [{
    type: "pie",
    showInLegend: true,
    toolTipContent: "{name}: <strong>{y} Seg</strong>",
    legendText: "{name}",
       dataPoints: [
      { y: parseInt(data1.t1), name: "Fase 1", exploded: true },
      { y: parseInt(data1.t2), name: "Fase 2",exploded: true },
      { y: parseInt(data1.t3), name: "Fase 3" ,exploded: true},
    ]
    
  }]
});
chart.render();
    }
});






      });
    </script>