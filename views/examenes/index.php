<?php
 include('../header.php'); ?>

    <div class="main">

        <div class="main-inner">

            <div class="container">

                <div class="row">

                    <div class="span12">

                        <div class="widget ">

                            <div class="widget-header">
                                <i class="icon-user"></i>
                                <h3>Examenes</h3>
                            </div>
                            <!-- /widget-header -->

                            <div class="widget-content">
<?php echo date('Y-m-d H:i:s'); ?>
                                <div class="tabbable">

                                    <?php 
                                    if ( $_SESSION['nivel'] == 1) {
                                       ?>
<ul class="nav nav-tabs">

                                        <li  class="active"><a href="#verUsers" data-toggle="tab">Lista de examenes</a></li>
                                        <li><a href="#verexamen" data-toggle="tab">Examenes Realizados</a></li>
                                    </ul>
                                       <?php  
                                    }else{
                                    ?>
<ul class="nav nav-tabs">
                                        <li >
                                            <a href="#formcontrols" data-toggle="tab">Registrar Examen</a>
                                        </li>
                                        <li  class="active"><a href="#verUsers" data-toggle="tab">Lista de examenes</a></li>
                                        <li><a href="#verexamen" data-toggle="tab">Examenes Realizados</a></li>
                                    </ul>


                                <?php } ?>
                                    



                              
                                    

                                    <br>

                                    <div class="tab-content">
                                        <div class="tab-pane  " id="formcontrols">
                                            <form id="edit-profile" class="" action="<?php echo API; ?>RegistroUsuario.php" method="POST">
                                                <center>
                                                	<a style="width:80%;height: 100px;text-align: center;"  href="crearExamen.php" class="btn btn-primary "><br>Crear Examen</a>
                                                </center>

                                            </form>
                                        </div>
                                        <!-- PANTALLA DE REGISTRO  -->

                                        <div class="tab-pane active " id="verUsers">

                                            <div class="widget-header"> <i class="icon-th-list"></i>
                                                <h3>Lista de examenes </h3>
                                            </div>
                                            <div class="widget-content">
                                                <table id="tabUser" class=" table table-striped table-bordered " style="text-align: center;" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th> Examen</th>
                                                            <th> Sección</th>
                                                            <th> fecha</th>
                                                            <th class="td-actions">opciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th> Examen</th>
                                                            <th> Sección</th>
                                                            <th> fecha</th>
                                                            <th class="td-actions">opciones</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                            </div>


                                        </div>
                                        <!-- PANTALLA DE VER USUARIOS -->
                            <div class="tab-pane  " id="verexamen">

                                            <div class="widget-header"> <i class="icon-th-list"></i>
                                                <h3>Lista de examenes </h3>
                                            </div>
                                            <div class="widget-content">
                                                <table id="tabExamen" class=" table table-striped table-bordered " style="text-align: center;" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th> Usuario</th>
                                                            <th> Documento</th>
                                                            <th> Examen</th>
                                                            <th> nota</th>
                                                            <th> tiempo </th>
                                                            <th class="td-actions">opciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th> Usuario</th>
                                                            <th> Documento</th>
                                                            <th> Examen</th>
                                                            <th> nota</th>
                                                            <th> tiempo </th>
                                                            <th class="td-actions">opciones</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                            </div>


                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- /widget-content -->

                        </div>
                        <!-- /widget -->

                    </div>
                    <!-- /span8 -->

                </div>
                <!-- /row -->

            </div>
            <!-- /container -->

        </div>
        <!-- /main-inner -->

    </div>


            <div id="infoexamen" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Información del examen</h3>
                </div>
                <div class="modal-body">
                    <center>
                    <img src="<?php echo RUTA; ?>img/examen.png" class="rounded mx-auto d-block" alt="usuario" width="200px">
                    </center>
                    <div id="infoexamen1"  >
                        
                    </div>    


                    

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                
                </div>
            </div>
    <!-- /main -->
    <?php include('../footer.php'); ?>