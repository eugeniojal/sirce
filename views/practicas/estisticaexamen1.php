<?php 

include('../header.php');
?>
<input type="hidden" id="examen" name="" value="<?php echo $_GET['examen']; ?>">
    <!-- /subnavbar -->
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content" >

                              <center>
                                  
                                  <div class="stats-box-title">Promedio general en este examen</div>
                    <i class="icon-edit" style="color:#3C3" id="nota"></i>
                    <div id="promedio"></div>
                              </center>
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart" id="examen"></i>
                                <h3>Promedio de nota por genero</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                               <div id="barras" style="height: 250px; width: 100%;"></div>
                             
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Promedio de nota por año</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                              <div id="dona" style="height: 250px; width: 100%;"></div>
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <div class="span12">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                                 <div id="lineas" style="height: 250px; width: 100%;"></div>
                              
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
<?php
include('../footer.php');
 ?>

<script type="text/javascript">
      $(document).ready(function(){

 var examen = $('#examen').val();

$.ajax({
  url: '../../api/calculos.php',
  type: 'POST',
  dataType: 'JSON',
  data: {examen:examen,tipo:9},
  success:function(data1){
var mujeres = [];    
var hombres = [];
for(var i= 0; i < data1.length; i++) {
if (data1[i]['sexo'] == 'F') {
hombres.push({"label":"MUJERES","y":parseFloat(data1[i]['avg']),"color":"#E88BED"});
}else{
hombres.push({"label":"HOMBRES","y":parseFloat(data1[i]['avg']),"color":"#2B4BDC"});  
}


}

console.log(hombres);
console.log(mujeres);

var chart = new CanvasJS.Chart("barras", {
  animationEnabled: true,
  theme: "light", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Promedio de notas"
  },
  axisY: {
    title: "Promedio de notas por genero"
  },
  data: [{        
    type: "column",  
    showInLegend: true, 

    dataPoints:hombres
  }]
});
chart.render();
    }
});

  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:10},
        success:function(data){



var parts = data['t1'].split(':');
var t1 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data['t2'].split(':');
var t2 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);
var parts = data['t3'].split(':');
var t3 =  parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60  +parseInt(parts[2]);



var chart = new CanvasJS.Chart("lineas", {
  animationEnabled: true,
  theme: "light", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Promedio de tiempo por fase"
  },
  axisY: {
    title: "Promedio de tiempo por fase"
  },
  data: [{        
    type: "column",  
    showInLegend: true, 
    indexLabel: "{y}.Seg",
    toolTipContent: "<b>Tiempo promedio:</b> {y}.Seg",
  dataPoints: [      
            { y: t1, label: "FASE 1" },
            { y: t2,  label: "FASE 2" },
            { y: t3,  label: "FASE 3" },
        ]

  }]
});
chart.render();


    }
        

  });
  


  $.ajax({
    url: '../../api/calculos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {examen:examen,tipo:11},
        success:function(data){

var array = [];
for(var i= 0; i < data.length; i++) {

array.push({"y":parseFloat(data[i]['y']),"label":data[i]['label']});
}
          
var chart = new CanvasJS.Chart("dona", {
  theme: "light2", 
  animationEnabled: true,
  title:{
    text: "Promedio por año",
    horizontalAlign: "left"
  },
  data: [{
    type: "doughnut",
    startAngle: 60,
    //innerRadius: 60,
    indexLabelFontSize: 17,
    indexLabel: "{label} Años - {y}",
    toolTipContent: "<b>Nota promedio:</b> {y}",
    dataPoints: array
  }]
});
chart.render();


    }
        

  });
        $.getJSON("../../api/promedio.php",{examen:examen,tipo:1},function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                    $("#promedio").append(V['promedio']);

                });
            }
        });
      });
    </script>