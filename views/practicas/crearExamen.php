<?php include('../header.php'); ?>

    <div class="main">

        <div class="main-inner">

            <div class="container">

                <div class="row">

                    <div class="span12">

                        <div class="widget ">

                            <div class="widget-header">
                                <i class="icon-user"></i>
                                <h3>Practica</h3>
                            </div>
                            <!-- /widget-header -->
                        <form action="../../api/guardarExamen1.php" method="POST" accept-charset="utf-8">

<input type="hidden" name="tipoexamen" value="1">

                            <div class="widget-content" id="Divtitulo">
                                <center >
                                  <?php if (isset($_REQUEST['mensaje'])): ?>
                                  <div class="alert  alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>ERROR!</strong> <?php echo $_REQUEST['mensaje']; ?>
                                  </div>
                                    
                                  <?php endif ?>

                                 <div class="control-group">
                                  <label class="control-label" for="titulo">Nombre de la Practica</label>
                                  <div class="controls">
                                  <input type="text" name="titulo" class="span6" id="titulo" value="Donga">
                                   </div>
                                </div>        
                                <div class="form-actions">
                                 <a href="index.php" class="btn">Atras</a>
                                 <a onclick="mostrarSeccion();" class="btn btn-success">Siguiente</a>
                                 </div>
                                </center>
                            </div>

                            <div id="Divseccion" class="widget-content"  style="display: none;">
                                <center>
                                   <div class="control-group">
                                  <label class="control-label" for="secciones">Sección a la cual aplicara la practica</label>
                                  <div class="controls">
                                   <select class="secciones form-control" id="secciones" name="secciones" required></select>
                                   </div>
                                </div>       
                                <div class="form-actions">
                                 <a onclick="atras(1);" class="btn">Atras</a>
                                   <a onclick="mostrarFechas();" class="btn btn-success">Siguiente</a>

                                 </div>
                                </center>
                            </div>
                            <div id="DivfechaF" class="widget-content"  style="display:none ;">
                                <center>
                                   <div class="control-group">
                                  <label class="control-label" for="secciones">Duración de la practica</label>
                                  <div class="controls">
                                  <label class="control-label" for="secciones">Minutos</label>
                                   <input type="number" name="tiempo" id="tiempo" value="" placeholder="" min="0" max="60" required="">
                                   </div>
                                </div>  
                                 <div class="control-group">
                                  <label class="control-label" for="secciones">Fecha de Inicio</label>
                                  <div class="controls">
                                    <input type="datetime-local" name="fechaF" id="fechaF" value="" min="<?php echo date('Y-m-d\TH:i:s'); ?>" placeholder="" required="">
                                   </div>
                                </div>       
                                <div class="form-actions">
                                 <a onclick="atras(2);"  class="btn">Atras</a>
                                 <a onclick="mostrarponde();" class="btn btn-success">Siguiente</a>
                                 </div>
                                </center>
                            </div>
                               <div id="DivNotas" class="widget-content"  style="display: none;">
                              <center>
                                   <div class="control-group">
                                  <label class="control-label" for="secciones">Ponderación de las fases</label>
                                           <div class="controls">
                                            <label class="radio inline">
                                            
                                         <input type="number"  name="ponde1" id="ponde1" min="1" max="20" class="span2" required> fases 1
                                                  </label>

                                                  <label class=" radio inline">
                                                   
                                                      <input type="number" name="ponde2" min="0" max="20" id="ponde2" class="span2" required> fases 2
                                                   
                                                  </label>
                                                <label class=" radio inline">
                                                   
                                                      <input type="number" name="ponde3" min="0" max="20" id="ponde3" class="span2" required> fases 3
                                                   
                                                  </label>
                                              </div>
                                </div>      
                                <div class="form-actions">
                                 <a onclick="atras(3);"  class="btn">Atras</a>
                                 <button  type="submit" class="btn btn-success">Guardar</button>
                                 </div>
                                </center>
                            </div>

                        </form>
                
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                    </div>
                    <!-- /span8 -->

                </div>
                <!-- /row -->

            </div>
            <!-- /container -->

        </div>
        <!-- /main-inner -->

    </div>
    <!-- /main -->
    <?php include('../footer.php'); ?>