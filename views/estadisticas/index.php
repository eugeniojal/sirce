<?php 

include('../header.php');
?>

    <!-- /subnavbar -->
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart" id="examen"></i>
                                <h3>Promedio de nota por genero</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                               <div id="barras" style="height: 250px; width: 100%;"></div>
                             
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Grafica de promedio de tiempo en cada fase</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                                 <div id="lineas" style="height: 250px; width: 100%;"></div>
                              
                                <!-- /line-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->

                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Promedio de nota por sección</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                              <div id="dona" style="height: 250px; width: 100%;"></div>
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Promedio por examen</h3>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
           <div id="vertical" style="height: 250px; width: 100%;"></div>
                                <!-- /-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
<?php
include('../footer.php');



 ?>