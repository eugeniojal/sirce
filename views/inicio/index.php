<?php 

include('../header.php');
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">

        <?php if ($_SESSION['nivel'] != 1): ?>
                      <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Bienvenidos al sistema</h3>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadisticas del sistemas </h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-user"></i> <span class="value" id="user11"></span><br>Estudiantes </div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class="icon-book"></i> <span class="value" id="profe"></span><br>Profesores</div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class="icon-list-ol"></i> <span class="value" id="examen"></span><br>Examenes</div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class=" icon-edit"></i> <span class="value" id="examenr"></span><br>Examenes terminados</div>
                    <!-- .stat --> 
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
            <?php else: ?>
<input type="hidden" name="" id="examen" value="0">

          <?php endif ?>

          <!-- /widget -->
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Calendario </h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div id='calendar'>
              </div>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
          
          <!-- /widget --> 
        </div>
        <!-- /span6 -->
        <div class="span6">
          
          <!-- /widget -->
          <?php if ($_SESSION['nivel'] != 1): ?>
                      <div class="widget">
            <div class="widget-header"> <i class="icon-signal"></i>
              <h3> Estadistica por Generos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
             <!--  <canvas id="area-chart" class="chart-holder" height="250" width="538"> </canvas> -->
             <div id="barras" style="height: 250px; width: 100%;"></div>
              <!-- /area-chart --> 
            </div>
            <!-- /widget-content --> 
          </div>
          <?php endif ?>

          <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Examenes Prendientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                                                <table id="tabUser" class=" table table-striped table-bordered " style="text-align: center;" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th> Examen</th>
                                                            <th> Sección</th>
                                                     
                                                            <th class="td-actions">opciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th> Examen</th>
                                                            <th> Sección</th>
                                                           
                                                            <th class="td-actions">opciones</th>
                                                        </tr>

                                                    </tfoot>
                                                </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 

          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
            <div id="infoexamen" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Información del examen</h3>
                </div>
                <div class="modal-body">
                    <center>
                    <img src="<?php echo RUTA; ?>img/examen.png" class="rounded mx-auto d-block" alt="usuario" width="200px">
                    </center>
                    <div id="infoexamen1"  >
                        
                    </div>    


                    

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                
                </div>
            </div>

<?php
include('../footer.php');



 ?>