<?php define('RUTA','http://localhost/sirce/public/'); ?>
<?php define('VISTA','http://localhost/sirce/views/'); ?>
<?php define('API','http://localhost/sirce/api/');
define('archivos','http://localhost/sirce/api/archivos/');
session_start();
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Sistema SIRCE</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo RUTA; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?php echo RUTA; ?>css/font-awesome.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>js/full-calendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/style.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/pages/dashboard.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/select2.min.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/all.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/sweetalert2.css" rel="stylesheet">
<link href="<?php echo RUTA; ?>css/chart.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<input type="hidden" id="idusuario" name="" value="<?php echo $_SESSION['id_user']; ?>">
<body>
	<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid"> 
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
    	<span class="icon-bar"></span>
    	<span class="icon-bar"></span>
    	<span class="icon-bar"></span> 
      </a>
      <div class="nav-collapse">
      	<div class="nav pull-left">
      		<img src="<?php echo RUTA; ?>img/c0775b7189ee425afd84af707c4c336f.jpg" alt="feyalegria" width="50px">
      	</div>
      	
      </div>
      <a class="brand" href="index.html">Sistema SIRCE</a>
  	

      <div class="nav-collapse">
        <ul class="nav pull-right">
<!--           <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li> -->
          <li  class="dropdown"><a style="font-size: 20px;" href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> <?php echo $_SESSION['nombre'];?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo VISTA; ?>usuarios/perfil.php">Perfil</a></li>
              <li><a href="http://localhost/sirce/index.php?logout">Salir</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <!-- <input type="text" class="search-query" placeholder="Search"> -->
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
      <li id="dash" class=""><a href="<?php echo VISTA; ?>inicio/"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>

        <li id="exam"><a href="<?php echo VISTA; ?>examenes/"><i class="icon-list-alt"></i><span>Examenes</span> </a> </li>
        <li id="practicas"><a href="<?php echo VISTA; ?>practicas/"><i class=" icon-play-circle"></i><span>Practicas</span> </a> </li>
<!--         <li><a href="guidely.html"><i class="icon-facetime-video"></i><span>App Tour</span> </a></li> -->
        
      <?php if ($_SESSION['nivel'] == 1){ ?>
        
      <li id="user" class=""><a href="<?php echo VISTA; ?>usuarios/perfil.php"><i class="shortcut-icon icon-user"></i><span>Perfil</span></a></li>
      <?php }else{?>
        <li  id="report"><a href="<?php echo VISTA; ?>estadisticas/"><i class="icon-bar-chart"></i><span>Reportes</span> </a> </li>
      <li id="user" class=""><a href="<?php echo VISTA; ?>usuarios/"><i class="shortcut-icon icon-user"></i><span>Usuarios</span></a></li>
      <li id="secc" ><a href="<?php echo VISTA; ?>secciones/"><i class=" icon-group"></i><span>Secciones</span> </a> </li>
      <?php } ?>

        <li  id="apoyo"><a href="<?php echo VISTA; ?>apoyo/"><i class=" icon-folder-open"></i><span>Apoyo</span> </a> </li>

<!--         <li><a href="shortcodes.html"><i class="icon-code"></i><span>Shortcodes</span> </a> </li> -->
<!--         <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="icons.html">Icons</a></li>
            <li><a href="faq.html">FAQ</a></li>
            <li><a href="pricing.html">Pricing Plans</a></li>
            <li><a href="login.html">Login</a></li>
            <li><a href="signup.html">Signup</a></li>
            <li><a href="error.html">404</a></li>
          </ul>
        </li> -->
      </ul>

        <p class="pull-right " ><?php echo date('Y-m-d H:i:s');?></p>

    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>