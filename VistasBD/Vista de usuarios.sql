CREATE OR REPLACE VIEW 
vista_usuarios AS 
SELECT usuarios.id_usuario,usuarios.nombre,usuarios.ci,usuarios.pass,usuarios.apellido,usuarios.fechaN,usuarios.sexo,
sesiones.nombre as 'seccion',usuarios.correo,usuarios.tlf,usuarios.tipo,usuarios.direccion,
CASE 
	WHEN usuarios.tipo = 1 THEN 'Estudiante'
	WHEN usuarios.tipo = 2 THEN 'Profesor'
	WHEN usuarios.tipo = 3 THEN 'Administrador'
	ELSE 'indefinido'
END AS tipoUsuario
 FROM usuarios
INNER JOIN sesiones
ON usuarios.seccion = sesiones.id



CREATE OR REPLACE VIEW PreguntasSinResponder AS SELECT preguntas.id_pregunta,preguntas.id_examen,preguntas.id_fase,preguntas.pregunta,preguntas.id_respuesta,repuestasestudiantes.id_estudiante as 'estudiante' 
FROM preguntas 
left join repuestasestudiantes on preguntas.id_pregunta=repuestasestudiantes.id_pregunta

CREATE OR REPLACE VIEW exameneslistos AS 
SELECT realizado.id_realizado,realizado.nota,realizado.tt,examen.titulo,realizado.id_usuario,usuarios.seccion,CONCAT(usuarios.nombre, ' ', usuarios.apellido) As 'Nombre'
FROM realizado inner join examen on realizado.id_examen = examen.id_examen INNER JOIN usuarios on realizado.id_usuario = usuarios.id_usuario
where examen.id_seccion = usuarios.seccion

CREATE OR REPLACE VIEW vista_examen AS 
SELECT * FROM examen inner JOIN sesiones on examen.id_seccion = sesiones.id

CREATE OR REPLACE VIEW dona AS 
SELECT sesiones.nombre,FORMAT(AVG(nota),2) as 'promedio'
FROM realizado
inner join examen on realizado.id_examen = examen.id_examen
inner join sesiones on examen.id_seccion  = sesiones.id
GROUP BY  sesiones.id

CREATE OR REPLACE VIEW line AS 
SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(t1))) as t1, SEC_TO_TIME(AVG(TIME_TO_SEC(t2))) as t2,  SEC_TO_TIME(AVG(TIME_TO_SEC(t3))) as t3 FROM realizado

CREATE OR REPLACE VIEW verticales AS 
SELECT examen.titulo ,FORMAT(AVG(nota),2) as 'promedio'
FROM realizado
inner join examen on realizado.id_examen = examen.id_examen
inner join sesiones on examen.id_seccion  = sesiones.id
GROUP BY  examen.id_examen  
ORDER BY `promedio`  ASC

CREATE OR REPLACE VIEW examenesProfesor AS 
select `realizado`.`id_realizado` AS `id_realizado`,`realizado`.`nota` AS `nota`,`realizado`.`tt` AS `tt`,`examen`.`titulo` AS `titulo`,`realizado`.`id_usuario` AS `id_usuario`,`usuarios`.`seccion` AS `seccion`,`usuarios`.`ci` AS `ci`,`examen`.`practica` AS `practica`,concat(`usuarios`.`nombre`,' ',`usuarios`.`apellido`) AS `Nombre` from ((`realizado` join `examen` on((`realizado`.`id_examen` = `examen`.`id_examen`))) join `usuarios` on((`realizado`.`id_usuario` = `usuarios`.`id_usuario`))) where (`examen`.`id_seccion` = `usuarios`.`seccion`)