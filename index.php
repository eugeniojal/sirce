<?php
// include the configs / constants for the database connection
//require_once("php/defaults/conexion.php");

// load the login class
require_once("models/login1.php");

// create a login object. when this object is created, it will do all login/logout stuff automatically
// so this single line handles the entire login process. in consequence, you can simply ...
$login = new models\Login();

// ... ask if we are logged in here:
if ($login->isUserLoggedIn() == true) {
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
   header("location: views/index.php");

} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    ?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>SIRCE</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="public/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="public/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="public/css/style.css" rel="stylesheet" type="text/css">
<link href="public/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
			<img src="public/img/c0775b7189ee425afd84af707c4c336f.jpg" alt="feyalegria" width="50px">				
			</a>
			<a class="brand" href="index.html">
				<h2>SIRCE</h2>				
			</a>			
			
<!-- 			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="signup.html" class="">
							Don't have an account?
						</a>
						
					</li>
					
					<li class="">						
						<a href="index.html" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>
				
			</div> --><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="index.php" method="post">
		
			<h1>Inicio de sesión</h1>		
			
			<div class="login-fields">
	<?php 
	if (isset($login)) {
		if ($login->errors) {
	?>
	<div class="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Error!</strong>
	<?php 

            foreach ($login->errors as $error) {
              echo $error;
            }

	 ?>
	 .
    </div>
<?php }} ?>
				<p>Por favor ingrese los datos!</p>

				<div class="field">
					<label for="username">Nombre de Usuario</label>
					<input type="email" id="username" name="username" value="" placeholder="Nombre de Usuario" class="login username-field" required />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Contraseña:</label>
					<input type="password" id="password" name="password" value="" placeholder="Contraseña" class="login password-field" required/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
<!-- 				<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Keep me signed in</label>
				</span> -->
									
				<button type="submit" name="login" class="button btn btn-success btn-large">Iniciar</button>
				
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->



<div class="login-extra">
	<a href="#">Reiniciar contraseña</a>
</div> <!-- /login-extra -->


<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
<?php } ?>